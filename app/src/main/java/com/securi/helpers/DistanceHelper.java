package com.securi.helpers;

import android.location.Location;

import com.android.volley.toolbox.JsonArrayRequest;
import com.securi.addiscpn.AppController;

/**
 * Created by Michael on 10/12/2017.
 */

public class DistanceHelper {

    public static double getDistanceFromLatLngInKm(double lat1, double lon1, double lat2, double lon2) {
        Location startPoint=new Location("locationA");
        startPoint.setLatitude(lat1);
        startPoint.setLongitude(((float)lon1));

        Location endPoint=new Location("locationA");
        endPoint.setLatitude(lat2);
        endPoint.setLongitude(((float)lon2));
        double distance= startPoint.distanceTo(endPoint);


        return distance;
    }

    public static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    public static  double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }
}
