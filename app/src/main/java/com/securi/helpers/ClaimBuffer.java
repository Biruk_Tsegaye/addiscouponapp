package com.securi.helpers;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Zelalem G. on 8/9/2017.
 */

public class ClaimBuffer {
    private static ClaimBuffer instance = new ClaimBuffer();

    private static JSONArray arr = new JSONArray();
    private  JSONArray claimsArray = new JSONArray();
    private  JSONObject claimsObject;
    ClaimChangeListener listner;

    public static ClaimBuffer getInstance(){
        return instance;
    }

    public void AddNewClaim(Activity context, JSONObject object){

        boolean duplicate = false;

        for (int i=0; i < arr.length(); i++){
            try {
                if (object.getInt("itemId") == arr.getJSONObject(i).getInt("itemId")) {
                    duplicate = true;
                    break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(!duplicate) {
            arr.put(object);
            Log.e("Added arrayXXXXX ", arr.toString());
        }
        claimsArray = arr;
        listner = (ClaimChangeListener) context;
        listner.onCLaimChanged(claimsArray);
    }

    public void RemoveClaim(Activity context, int _itemId){
        int _position = 0;
        for (int i=0; i < arr.length(); i++){
            try {
                if (_itemId == arr.getJSONObject(i).getInt("itemId")) {
                    _position = i;
                    break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        //TODO: NOT COMPATIBLE WITH OLD DEVICES
        //arr.remove(_position);
        arr = RemoveJSONArray(arr, _position);
        claimsArray = arr;

        listner = (ClaimChangeListener) context;
        listner.onCLaimChanged(claimsArray);
    }

    public static JSONArray RemoveJSONArray( JSONArray jarray,int pos) {

        JSONArray Njarray=new JSONArray();
        try{
            for(int i=0;i<jarray.length();i++){
                if(i!=pos)
                    Njarray.put(jarray.get(i));
            }
        }catch (Exception e){e.printStackTrace();}
        return Njarray;
    }

    public JSONArray GetClaims(Activity context){
        if (claimsArray == null)
            claimsArray = new JSONArray();
        return claimsArray;
    }

    public JSONArray ResetClaim(Activity context){
        arr = null;
        arr = new JSONArray();
        claimsArray = arr;

        listner = (ClaimChangeListener) context;
        listner.onCLaimChanged(claimsArray);

        return claimsArray;
    }

    public void BroadcastDataChange(Activity context){
        listner = (ClaimChangeListener) context;
        listner.onCLaimChanged(claimsArray);
    }

    public interface ClaimChangeListener {
        void onCLaimChanged(JSONArray array);
    }
}
