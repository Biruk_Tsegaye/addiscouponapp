package com.securi.helpers;

import java.io.Serializable;

/**
 * Created by Michael on 7/21/2017.
 */

public class RetailerModel  implements Serializable{
    private long id;
    String name,logoPath,retailerCategory,picName,phoneNumber,email,locationDetails = "", city;
    double latitude,longitude;
    long categoryId;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocationDetails() {
        return locationDetails;
    }

    public void setLocationDetails(String locationDetails) {
        this.locationDetails = locationDetails;
    }

    public RetailerModel(){
    }

    public RetailerModel(String name, String retailerCategory, double latitude, double longitude){
        this.name= name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.retailerCategory = retailerCategory;
    }

    public RetailerModel(String name, String retailerCategory, double latitude, double longitude, String locationDetails){
        this.name= name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.retailerCategory = retailerCategory;
        this.locationDetails = locationDetails;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getRetailerCategory() {
        return retailerCategory;
    }

    public void setRetailerCategory(String retailerCategory) {
        this.retailerCategory = retailerCategory;
    }

    public void setId(long l){
        this.id = l;
    }

    public long getId(){
        return this.id;
    }


    public String getPicName() {
        return picName;
    }

    public void setPicName(String picName) {
        this.picName = picName;
    }


    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }
}
