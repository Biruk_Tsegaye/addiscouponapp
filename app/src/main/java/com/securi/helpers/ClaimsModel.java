package com.securi.helpers;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Zelalem G. on 7/31/2016.
 * Database nmodel for creating table
 * storing claims and track redeeming.
 */
public class ClaimsModel extends SugarRecord {
    private Long id;
    String itemName;
    double originalPrice;
    double discount;
    int claimCode;
    String retailorName;
    boolean isRedeemed;
    Date claimDate;
    String picName;
    String email;
    String phone;
    String location;
    String city;
    String category;
    double latitude;
    double longitude;

    @Ignore
    int count;

    public ClaimsModel(){

    }

    public ClaimsModel(String _itemName, double _OriginalPrice, double _Discount, int _claimCode, boolean _isRedeemed){
        this.itemName = _itemName;
        this.originalPrice = _OriginalPrice;
        this.discount = _Discount;
        this.claimCode = _claimCode;
        this.isRedeemed = _isRedeemed;
        this.claimDate = new Date();
    }

    public ClaimsModel(String _itemName, double _OriginalPrice, double _Discount, int _claimCode, String _retailorName, boolean _isRedeemed, String _picName, int uj){
        this.itemName = _itemName;
        this.originalPrice = _OriginalPrice;
        this.discount = _Discount;
        this.claimCode = _claimCode;
        this.isRedeemed = _isRedeemed;
        this.claimDate = new Date();
        this.retailorName = _retailorName;
        picName = _picName;

    }

    public ClaimsModel(String _itemName, double _OriginalPrice, double _Discount, int _claimCode,
                       String _retailorName, boolean _isRedeemed, String _picName, String _email,
                       String _phone, String _location, String _city, String _category,
                       double _latitude, double _longitude){
        this.itemName = _itemName;
        this.originalPrice = _OriginalPrice;
        this.discount = _Discount;
        this.claimCode = _claimCode;
        this.isRedeemed = _isRedeemed;
        this.claimDate = new Date();
        this.retailorName = _retailorName;
        picName = _picName;

        email = _email;
        phone = _phone;
        location = _location;
        city = _city;
        category = _category;
        latitude = _latitude;
        longitude = _longitude;
    }

    //------geters & setters---------\\

    public int GetClaimId(){
        return this.claimCode;
    }
    public int getClaimCode(){
        return this.claimCode;
    }

    public String getItemName(){
        return this.itemName;
    }
    public String getPicName(){
        return this.picName;
    }


    public String GetRetailorName(){
        return this.retailorName;
    }
    public double getOriginalPrice(){
        return this.originalPrice;
    }
    public double GetCouponPrice(){
        return CpnUtils.RoundMyDoubleDigits(this.originalPrice - this.discount);
    }
    public double getDiscount(){
        return this.discount;
    }
    public boolean GetIsRedeemed() {
        return this.isRedeemed;
    }
    public Date getClaimDate() {
        return this.claimDate;
    }

    public String getEmail(){
        return this.email;
    }
    public String getPhone(){
        return this.phone;
    }
    public String getLocation(){
        return this.location;
    }
    public String getCity(){
        return this.city;
    }
    public String getCategory(){
        return this.category;
    }
    public double getLatitude(){
        return this.latitude;
    }
    public double getLongitude(){
        return this.longitude;
    }

    // === ------ SETTERS ------ === \\

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public void setClaimCode(int claimCode) {
        this.claimCode = claimCode;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public void setDiscount(double dicount) {
        this.discount = discount;
    }

    public void setClaimDate(Date claimDate) {
        this.claimDate = claimDate;
    }
    public void setDiscount(boolean isRedeemed) {
        this.isRedeemed = isRedeemed;
    }
}
