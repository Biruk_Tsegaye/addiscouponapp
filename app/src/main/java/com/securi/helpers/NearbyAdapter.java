package com.securi.helpers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.securi.addiscpn.BusinessDetails;
import com.securi.addiscpn.MapActivity;
import com.securi.addiscpn.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michael on 7/25/2017.
 */

public class NearbyAdapter extends RecyclerView.Adapter<NearbyAdapter.RetailersViewHolder>{
    Context context;
    TextView tv_retailerName;
    ImageView retailerLogo;
    ImageButton showOnMapButton;
    List<RetailerModel> retailerModelList;
    JSONArray nearbyBusinesses;
    URL url;
    ImageLoader imageLoader;
    ProgressDialog progressDialog;
    String retailerName;

    public NearbyAdapter(Context con, JSONArray jsonArray){
        this.context = con;
        retailerModelList = new ArrayList<RetailerModel>();
        this.progressDialog = new ProgressDialog(context,R.style.MyTheme);
        this.progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        this.nearbyBusinesses = jsonArray;
        progressDialog.show();
        populateList();
//        progressDialog.dismiss();

    }

    @Override
    public RetailersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.retailers_list_item,parent,false);
        tv_retailerName = (TextView) itemView.findViewById(R.id.retailer_name);
        retailerLogo = (ImageView) itemView.findViewById(R.id.retailer_logo);
        showOnMapButton = (ImageButton) itemView.findViewById(R.id.show_on_map);
        return new RetailersViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RetailersViewHolder holder, final int position) {
        final RetailerModel model = retailerModelList.get(position);
        tv_retailerName = (TextView) holder.getItemView().findViewById(R.id.retailer_name);
        tv_retailerName.setText(retailerModelList.get(position).getName());

        View itemView = holder.getItemView();
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BusinessDetails.class);
                intent.putExtra("retailerModel",retailerModelList.get(position));
                context.startActivity(intent);
            }
        });

        showOnMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//            Intent googleMapsIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:" + model.getLatitude() + "," + model.getLongitude() ));
//            if(googleMapsIntent.resolveActivity(context.getPackageManager()) != null){
//                context.startActivity(googleMapsIntent);
//            }else{
//                CpnUtils.ShowErrorDialog(context,"Error","You don't have google maps installed on your device");
//            }
                Intent intent = new Intent(context, MapActivity.class);
                intent.putExtra("latitude",model.getLatitude());
                intent.putExtra("longitude",model.getLongitude());
                intent.putExtra("name",model.getName());
                context.startActivity(intent);
            }
        });

        //TODO: get actual picName from the json

        String url =  Constants.RETAILER_IMAGE + retailerModelList.get(position).getPicName() + ".jpg";



        Picasso.with(context).load(url).placeholder(R.mipmap.background).into(retailerLogo);
    }

    public void populateList(){

        SharedPrefs prefs = new SharedPrefs(this.context);

        if(prefs.getLanguage().equals("am")){
            retailerName = "retailerNameAmharic";
        }else{
            retailerName = "retailerNameAmharic";
        }

        try {
            retailerModelList = new ArrayList<>();

            for(int i=0; i < nearbyBusinesses.length();i++){
                JSONObject obj = nearbyBusinesses.getJSONObject(i);
                Log.d("VOLLEYY",obj.toString());
                RetailerModel currentModel = new RetailerModel();

                currentModel.setId(Long.valueOf(obj.getString("$id")));
                currentModel.setName(obj.getString(retailerName).replace('_',' '));
                currentModel.setLatitude(obj.getDouble("latitude"));
                currentModel.setPicName(obj.getString("picNameRetailer"));
                currentModel.setLongitude(obj.getDouble("longtude"));
                currentModel.setLocationDetails(obj.getString("locationDetails"));
                currentModel.setPhoneNumber(obj.getString("headContactPhone"));
                currentModel.setCity(obj.getString("city"));
                currentModel.setEmail(obj.getString("headContactEmail"));
                currentModel.setCategoryId(Long.valueOf(obj.getString("categoryId")));
                String category = obj.getJSONObject("retailerCategory").getString("categoryName");
                currentModel.setRetailerCategory(category);
//                        Log.d("CURRENT_MODEL",obj.toString());
                if(!retailerModelList.contains(currentModel))
                    retailerModelList.add(currentModel);

                if(currentModel.getPicName() != null)
                    Glide.with(context).load(Constants.BASE_URL + Constants.RETAILER_IMAGE + currentModel.getPicName()+".jpg");

                notifyDataSetChanged();
            }
            progressDialog.dismiss();
        } catch (JSONException e) {
            Toast.makeText(context,"json exception" + e.getMessage(),Toast.LENGTH_LONG).show();
            NearbyAdapter.this.progressDialog.dismiss();
            progressDialog.dismiss();

        }

    }

    @Override
    public int getItemCount() {
        return retailerModelList.size();
    }

    public class RetailersViewHolder extends RecyclerView.ViewHolder{
        View itemView;
        public RetailersViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
        }
        public View getItemView(){
            return itemView;
        }
    }

    public void setRetailerModelList(List<RetailerModel> retailerModelList) {
        this.retailerModelList = retailerModelList;
    }
}
