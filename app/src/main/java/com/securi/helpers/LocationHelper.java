package com.securi.helpers;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import java.security.acl.Permission;

/**
 * Created by Michael on 10/13/2017.
 */

public class LocationHelper extends ActivityCompat implements LocationListener {
    Context context;
    LocationManager locationManager;
    Location userLocation;

    public LocationHelper(Context context){
        this.context = context;

        try {
            if (LocationPermissionIsOk())
                InitializeLocationObject();
            else
                ActivityCompat.requestPermissions((Activity) context,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean LocationPermissionIsOk()
    {
        if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED)
            return true;
        else
            return false;
    }


    void InitializeLocationObject(){
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        userLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
    }


    public double getUserLatitude() throws Exception{
        return  userLocation.getLatitude();
    }

    public double getUserLongitude() throws Exception{
        return userLocation.getLongitude();
    }

    public Location getUserLocation() throws Exception{
        return userLocation;
    }

    @Override
    public void onLocationChanged(Location location) {
        this.userLocation = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
