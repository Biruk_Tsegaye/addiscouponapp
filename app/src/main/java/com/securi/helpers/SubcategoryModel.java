package com.securi.helpers;

/**
 * Created by Michael on 8/7/2017.
 */

class SubcategoryModel {
    private long id;
    String subcategoryName;
    String description;
    String picName;
    int categoryId;


    public SubcategoryModel(long id, String categoryName, String description,int categoryId) {
        this.subcategoryName = categoryName;
        this.id = id;
        this.description = description;
        this.categoryId = categoryId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getSubcategoryName() {
        return subcategoryName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public void setSubcategoryName(String subcategoryName) {

        this.subcategoryName = subcategoryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPicName() {
        return picName;
    }

}