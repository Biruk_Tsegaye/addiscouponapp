package com.securi.helpers;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.securi.addiscpn.Home;
import com.securi.addiscpn.R;
import com.securi.addiscpn.Subcategories;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.securi.helpers.Constants.BASE_URL;
import static com.securi.helpers.Constants.GET_ITEM_CAT_BY_BUSI_CAT;

/**
 * Created by Michael on 7/26/2017.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder>{
    Context context;
    List<CategoryModel> categoryList = new ArrayList<>();
    TextView categoryName, categoryDescription;
    ImageView categoryIcon;

    public static final String CATEGORY = "Category";



//    public static final int abc = Context.get;

    Typeface cursiveFont, casualFont;


    public CategoriesAdapter(Context con){
        this.context = con;

        String SUPERMARKET = con.getResources().getString(R.string.supermarket)
                ,RESTAURANT = con.getResources().getString(R.string.restaurant)
                ,SPA = con.getResources().getString(R.string.spa)
                ,HOTEL_AND_TRAVEL = con.getResources().getString(R.string.travel_and_hotel)
                ,EVENTS = con.getResources().getString(R.string.event)
                ,GETAWAYS = con.getResources().getString(R.string.getway)
                ,FITNESS = con.getResources().getString(R.string.fitnes)
                ,OTHER = con.getResources().getString(R.string.other);

        categoryList.add(new CategoryModel(1,SUPERMARKET,context.getResources().getString(R.string.supermarket_detail),R.mipmap.supermarket));
        categoryList.add(new CategoryModel(3,RESTAURANT,context.getResources().getString(R.string.restaurant_detail),R.mipmap.restaurants));
        categoryList.add(new CategoryModel(5,SPA,context.getResources().getString(R.string.spa_detail),R.mipmap.spa));
        categoryList.add(new CategoryModel(7,HOTEL_AND_TRAVEL,context.getResources().getString(R.string.travel_and_hotel_detail),R.mipmap.tavel_and_hotel));
        categoryList.add(new CategoryModel(9,EVENTS,context.getResources().getString(R.string.event_detail),R.mipmap.events));
        categoryList.add(new CategoryModel(11,GETAWAYS,context.getResources().getString(R.string.getway_detail),R.mipmap.getaways));
        categoryList.add(new CategoryModel(13,FITNESS,context.getResources().getString(R.string.fitnes_detail),R.mipmap.fitness));
        categoryList.add(new CategoryModel(15,OTHER,context.getResources().getString(R.string.other_detail),R.mipmap.others));

        AssetManager assetManager = con.getApplicationContext().getAssets();

        cursiveFont = Typeface.createFromAsset(assetManager,String.format(Locale.US, "fonts/%s", "dancing_script.ttf"));
        casualFont = Typeface.createFromAsset(assetManager,String.format(Locale.US, "fonts/%s", "coming-soon.regular.ttf"));
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.category_list_item,parent,false);
        categoryName = (TextView) v.findViewById(R.id.category_name);
        categoryDescription = (TextView) v.findViewById(R.id.category_description);
        categoryIcon = (ImageView) v.findViewById(R.id.category_icon);
        return new CategoriesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final CategoriesViewHolder holder, final int position) {
        final CategoryModel model = categoryList.get(position);
        categoryName.setText(model.getName());
        categoryDescription.setText(model.getDescription());
        categoryIcon.setImageDrawable(context.getResources().getDrawable(model.getIcon()));
        View itemView = holder.getItemView();

        categoryName.setTypeface(cursiveFont);
        categoryDescription.setTypeface(casualFont);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (position > 0){
                Intent homeIntent = new Intent(context,Home.class);

                String url = Constants.ITEM_BY_BUSINESS + model.getId() + "/";

                String tabs_url = String.valueOf(BASE_URL+ GET_ITEM_CAT_BY_BUSI_CAT + model.getId());

                homeIntent.putExtra("HOME_URL",url);
                homeIntent.putExtra("TAB_URL",tabs_url);
                homeIntent.putExtra("CAT_ID",model.getId().intValue());
                homeIntent.putExtra("category","1");
                homeIntent.putExtra(CategoriesAdapter.CATEGORY,model.getName());
                homeIntent.putExtra("LayoutType","wide");
                homeIntent.putExtra("icon",model.getIcon());
                context.startActivity(homeIntent);


            } else {
                Intent supermarketSubcategories = new Intent(context, Subcategories.class);
                context.startActivity(supermarketSubcategories);
            }
            }
        });

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public void addCategory(CategoryModel model){
        categoryList.add(model);
        notifyDataSetChanged();
    }

    public void removeCategory(CategoryModel model){
        if(categoryList.contains(model)){
            categoryList.remove(model);
        }
    }

    class CategoriesViewHolder extends ViewHolder{
        View itemView;

        public CategoriesViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
        }

        public View getItemView(){
            return itemView;
        }
    }

}
