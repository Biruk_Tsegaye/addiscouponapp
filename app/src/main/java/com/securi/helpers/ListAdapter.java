package com.securi.helpers;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.securi.addiscpn.AppController;
import com.securi.addiscpn.BusinessDetails;
import com.securi.addiscpn.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Level1 on 7/26/2016.
 */
public class ListAdapter extends BaseAdapter {
    Activity context;
    JSONArray jsonArray;
    ImageLoader imageLoader;
    String itemName,retailerName;
    SharedPrefs prefs;

    public ListAdapter(Activity context, JSONArray _jsonArray) {
        this.context = context;
        jsonArray = _jsonArray;
    }


    @Override
    public int getCount() {
        return jsonArray.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0; //modelList.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View myView = null;
        prefs = new SharedPrefs(this.context);
//
        if(prefs.getLanguage().equals("am")){
            itemName = "itemNameAmharic";
            retailerName = "retailerNameAmharic";
        }else {
            itemName = "itemName";
            retailerName = "retailerName";
        }



        try{

            CpnUtils utils = new CpnUtils();
            LayoutInflater inflater = context.getLayoutInflater();
            myView = inflater.inflate(R.layout.list_item_style, null);
            final JSONObject object = jsonArray.getJSONObject(position);

            TextView txtItemName = (TextView) myView.findViewById(R.id.txt_ItemName);
            TextView txtOrigPrice = (TextView) myView.findViewById(R.id.txt_originalPrice);
            TextView txtCpnPrice = (TextView) myView.findViewById(R.id.txt_cpnPrice);
            ImageView thumbNail = (ImageView) myView.findViewById(R.id.img_ItemPic);
            TextView tv_retailerName = (TextView) myView.findViewById(R.id.txt_busines);
            tv_retailerName.setText(jsonArray.getJSONObject(position).getJSONObject("retailer").getString(retailerName));

            tv_retailerName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent detailsIntent = new Intent(context, BusinessDetails.class);
                    try {
                        detailsIntent.putExtra("retailerJson",jsonArray.getJSONObject(position).getJSONObject("retailer").toString());
                        context.startActivity(detailsIntent);
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }
                }
            });

            if(imageLoader == null){
                imageLoader = AppController.getInstance().getImageLoader();
            }

//            thumbNail.setImageUrl(Constants.IMAGE_URL + object.getString("picName"), imageLoader);
            Glide.with(context)
                    .load(Constants.IMAGE_URL + object.getString("picName"))
                    .into(thumbNail);

            ImageButton delete = (ImageButton) myView.findViewById(R.id.delete_item);

            delete.setTag(position);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        ClaimBuffer.getInstance().RemoveClaim(context, object.getInt("itemId"));
                        jsonArray = ClaimBuffer.getInstance().GetClaims(context);
                        notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            txtItemName.setText(object.getString(itemName));
            txtOrigPrice.setText(context.getString(R.string.j_detail_ordinary) + String.valueOf(object.getDouble("originalPrice")));
            txtCpnPrice.setText(context.getString(R.string.j_detail_addis) + String.valueOf(CpnUtils.RoundMyDoubleDigits(object.getDouble("originalPrice") - object.getDouble("discount"))));

            txtOrigPrice.setPaintFlags(txtOrigPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return myView;
    }

}
