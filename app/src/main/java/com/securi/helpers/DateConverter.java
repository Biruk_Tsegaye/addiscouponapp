package com.securi.helpers;

import java.util.Date;

public class DateConverter {
	
	public DateConverter()
	{
	}

	public static String toGregorian(String ethDate)
	{
        String[] pDate = ethDate.split("-");

      //  String[] EtDate = pDate[2].Split('-');
        int Day = Integer.valueOf(pDate[0]);
        int Month = Integer.valueOf(pDate[1]);
        int Year = Integer.valueOf(pDate[2]);

        Date gregorianDate = new Date();// null;
		int gDay = 0;
		int gMonth = 0;
		int gYear = 0;
		if (Month < 1 || Month > 13 || Day < 1 || Day > 30 || Year < 1)
		{
            return null;
		}
		int newYearDay = whenIsEthNewYearInGregMonth(Year);
		int gregorianYear = Year + 7;
		int[] gregorianMonths = {0, 30, 31, 30, 31, 31, 28, 31, 30, 31, 30, 31, 31, 30};
		int nextYear = gregorianYear + 1;
		if (nextYear % 4 == 0 && nextYear % 100 != 0 || nextYear % 400 == 0)
		{
			gregorianMonths[6] = 29;
		}
		int until = (Month - 1) * 30 + Day;
		if (until <= 37 && Year <= 1575)
		{
			until += 28;
			gregorianMonths[0] = 31;
		}
		else
		{
			until += newYearDay - 1;
		}
		until = (Year - 1) % 4 != 3 || Month != 13 ? until :++until;
		int m = 0;
		for (int i = 0; i < gregorianMonths.length; i++)
		{
			if (until <= gregorianMonths[i])
			{
				m = i;
				gDay = until;
				break;
			}
			m = i;
			until -= gregorianMonths[i];
		}

		int[] order = {8, 9, 10, 11, 12, 1, 2, 3, 4, 5, 6, 7, 8, 9};
		gYear = m <= 4 ? gregorianYear :++gregorianYear;
		gMonth = order[m];

        String gregynDate = null;

        if (gDay != 0 && gMonth != 0 && gYear != 0)
        {
            String dd = String.valueOf(gDay);
            String mm = String.valueOf(gMonth);
            if (gDay < 10)
                dd = "0" + String.valueOf(gDay);
            if (gMonth < 10)
                mm = "0" + String.valueOf(gMonth);

            gregynDate = gYear + "-" + mm + "-" + dd;
          //  gregorianDate = new DateTime(gDay, gMonth, gYear);
        }

         

        return gregynDate;
	}

	public static String toEthiopianDate(Date gregorianDate)
	{
        Date ethDate = new Date();// null;
		if (gregorianDate.getMonth() < 1 || gregorianDate.getMonth() > 12 || gregorianDate.getDay() < 1 || gregorianDate.getDay() > 31 || gregorianDate.getYear() < 1)
		{
			return null;
		}
		int[] gregorianMonths = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		int lastDateJulianCalendarInGregorian = 0x8cf9a;
		for (int i = 0; i < 10; i++)
		{
			lastDateJulianCalendarInGregorian += gregorianMonths[i];
		}

		int startingGregorianCalendarDate = 3286;
		for (int i = 0; i < gregorianDate.getMonth(); i++)
		{
			startingGregorianCalendarDate += gregorianMonths[i];
		}

		int thisDate = gregorianDate.getYear() * 365 + gregorianDate.getDay();
		for (int i = 0; i < gregorianDate.getMonth(); i++)
		{
			thisDate += gregorianMonths[i];
		}

		if (thisDate < startingGregorianCalendarDate)
		{
			return null;
		}
		if (gregorianDate.getMonth() == 10 && gregorianDate.getDay() >= 5 && gregorianDate.getDay() <= 14 && gregorianDate.getYear() == 1582)
		{
			return null;
		}
		if (gregorianDate.getYear() % 4 == 0 && gregorianDate.getYear() % 100 != 0 || gregorianDate.getYear() % 400 == 0)
		{
			gregorianMonths[2] = 29;
		}
		int gUntil = 0;
		for (int i = 0; i < gregorianDate.getMonth(); i++)
		{
			gUntil += gregorianMonths[i];
		}

		gUntil += gregorianDate.getDay();
		int eYear = gregorianDate.getYear() - 8;
		int[] ethiopianMonths = {30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 5};
		ethiopianMonths[12] = eYear % 4 != 3 ? 5 : 6;
		int eLeapYears = 0;
		if (thisDate <= lastDateJulianCalendarInGregorian)
		{
			eLeapYears = (eYear - 1) % 4 != 3 ? 0 : 1;
		}
		else
		{
			for (int i = 1; i < eYear; i++)
			{
				if (i % 4 == 3)
				{
					eLeapYears++;
				}
			}

		}
		int gLeapYears = 0;
		if (thisDate <= lastDateJulianCalendarInGregorian)
		{
			gLeapYears = (gregorianDate.getYear() - 1) / 4;
		}
		else
		{
			gLeapYears = ((gregorianDate.getYear() - 1) / 4 - (gregorianDate.getYear() - 1) / 100) + (gregorianDate.getYear() - 1) / 400;
		}
		int betweenMeskeremOneandJanuaryOne = 125;
		if (thisDate <= lastDateJulianCalendarInGregorian)
		{
			betweenMeskeremOneandJanuaryOne -= eLeapYears;
		}
		else
		{
			betweenMeskeremOneandJanuaryOne -= eLeapYears - gLeapYears;
		}
		int eUntil = betweenMeskeremOneandJanuaryOne + gUntil;
		int eMonth = 0;
		int eDate = eUntil;
		int numberOfMonths = 0;
		for (; eUntil > ethiopianMonths[eMonth]; eMonth %= 13)
		{
			if (++numberOfMonths == 13)
			{
				eYear++;
			}
			eUntil -= ethiopianMonths[eMonth++];
			eDate = eUntil;
		}

		eMonth++;
		//ethDate = new DateTime(eDate, eMonth, eYear);
        String ETDate = eDate + "-" + eMonth + "-" + eYear;
		//return ethDate;
        return ETDate;


	}

	protected static int whenIsEthNewYearInGregMonth(int eYear)
	{
		int eLeapPears = eYear / 4;
		int gYear = eYear + 7;
		int gLeapYears = (gYear / 4 - gYear / 100) + gYear / 400;
		int firstEthNewYearOccuredInGregorianAugustCE = 29;
		int newYearDate = (firstEthNewYearOccuredInGregorianAugustCE + (eLeapPears - gLeapYears)) % 31;
		return newYearDate;
	}


}
