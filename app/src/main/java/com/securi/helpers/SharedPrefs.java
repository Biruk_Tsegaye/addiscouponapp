package com.securi.helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Zelalem G. on 7/25/2016.
 */

public class SharedPrefs {
    Context context;
    public static final String SEED  = "seed"
            ,WALKTHROUGH_SHOWN="walkthrough_shown"
            ,ITEM_LIST="item_list";

    public SharedPrefs(Context _context){
        this.context = _context;
    }

    public boolean getRegistrationState(){
        boolean isRegistered = false;

        SharedPreferences prefs = context.getSharedPreferences("REG", Context.MODE_PRIVATE);
        if(prefs.getBoolean("REG", false))
            isRegistered = true;

        return isRegistered;
    }

    public void setUserAsRegistered(){
        SharedPreferences.Editor editor = context.getSharedPreferences("REG", Context.MODE_PRIVATE).edit();
        editor.putBoolean("REG", true);
        editor.commit();
    }

    public void setDataInitialized(){
        SharedPreferences.Editor editor = context.getSharedPreferences(SEED, Context.MODE_PRIVATE).edit();
        editor.putBoolean(SEED, true);
        editor.commit();
    }

    public void saveUserName(String name) {
        SharedPreferences.Editor editor = context.getSharedPreferences("NAME", Context.MODE_PRIVATE).edit();
        editor.putString("NAME", name);
        editor.commit();
    }

    public String getUserName(){
        SharedPreferences prefs = context.getSharedPreferences("NAME", Context.MODE_PRIVATE);
        return prefs.getString("NAME", "");
    }

    public void saveUserId(int uid) {
        SharedPreferences.Editor editor = context.getSharedPreferences("UID", Context.MODE_PRIVATE).edit();
        editor.putInt("UID", uid);
        editor.commit();
    }

    public int getUserId(){
        SharedPreferences prefs = context.getSharedPreferences("UID", Context.MODE_PRIVATE);
        return prefs.getInt("UID", 0);
    }

    public void setLanguage(String language){
        SharedPreferences.Editor editor = context.getSharedPreferences("LANG", Context.MODE_PRIVATE).edit();
        editor.putString("LANG", language);
        editor.apply();
    }
    public String getLanguage(){
        SharedPreferences prevlan = context.getSharedPreferences("LANG",Context.MODE_PRIVATE);
        return prevlan.getString("LANG", "am");
    }

    public void setWalkThroughShown(boolean bool){
        SharedPreferences.Editor editor = context.getSharedPreferences(WALKTHROUGH_SHOWN, Context.MODE_PRIVATE).edit();
        editor.putBoolean(WALKTHROUGH_SHOWN, true);
        editor.commit();
    }

    public boolean isWalkThroughShown(){
        SharedPreferences.Editor editor = context.getSharedPreferences(WALKTHROUGH_SHOWN, Context.MODE_PRIVATE).edit();
        return context.getSharedPreferences(WALKTHROUGH_SHOWN,Context.MODE_PRIVATE).getBoolean(WALKTHROUGH_SHOWN,false);
    }

    public void setMinimumVersion(double minimumVersion){
        SharedPreferences.Editor editor = context.getSharedPreferences("MINIMUMVERSION", Context.MODE_PRIVATE).edit();
        editor.putFloat("MINVERSION", (float) minimumVersion);
        editor.commit();
    }
    public float getminimumVersion(){

        SharedPreferences prefs = context.getSharedPreferences("MINIMUMVERSION", Context.MODE_PRIVATE);
        return prefs.getFloat("MINVERSION", (float) 0);

    }
    public void setCurrentAppVersion(double CurrentAppVersion){
        SharedPreferences.Editor editor = context.getSharedPreferences("CurrentAppVersion", Context.MODE_PRIVATE).edit();
        editor.putFloat("CurrentAppVersion", (float) CurrentAppVersion);
        editor.commit();
    }
    public float getCurrentAppVersion(){

        SharedPreferences prefs = context.getSharedPreferences("CurrentAppVersion", Context.MODE_PRIVATE);
        return prefs.getFloat("CurrentAppVersion", (float) 0);

    }
    public void setPromoCodeActive(boolean promoCodeActive){
        SharedPreferences.Editor editor = context.getSharedPreferences("PromoCodeActive", Context.MODE_PRIVATE).edit();
        editor.putBoolean("PromoCodeActive", promoCodeActive);
        editor.commit();
    }
    public boolean IsPromoCodeActive(){
        SharedPreferences.Editor editor = context.getSharedPreferences("PromoCodeActive", Context.MODE_PRIVATE).edit();
        return context.getSharedPreferences("PromoCodeActive",Context.MODE_PRIVATE).getBoolean("PromoCodeActive",false);
    }
    public void setReferalProgActive(boolean referalProgActive){
        SharedPreferences.Editor editor = context.getSharedPreferences("ReferalProgActive", Context.MODE_PRIVATE).edit();
        editor.putBoolean("ReferalProgActive", referalProgActive);
        editor.commit();
    }
    public boolean IsReferalProgActive(){
        SharedPreferences.Editor editor = context.getSharedPreferences("ReferalProgActive", Context.MODE_PRIVATE).edit();
        return context.getSharedPreferences("ReferalProgActive",Context.MODE_PRIVATE).getBoolean("ReferalProgActive", false);
    }
    public void setIsActive(boolean isActive){
        SharedPreferences.Editor editor = context.getSharedPreferences("isActive", Context.MODE_PRIVATE).edit();
        editor.putBoolean("isActive", isActive);
        editor.commit();
    }
    public boolean IsActive(){
        SharedPreferences.Editor editor = context.getSharedPreferences("isActive", Context.MODE_PRIVATE).edit();
        return context.getSharedPreferences("isActive",Context.MODE_PRIVATE).getBoolean("isActive", false);
    }

    public void setShowInfoDialog(boolean show){
        SharedPreferences.Editor editor = context.getSharedPreferences("showInfoDialog", Context.MODE_PRIVATE).edit();
        editor.putBoolean("show",show);
        editor.commit();
    }

    public boolean isInfoDialogEnabled(){
        return  context.getSharedPreferences("showInfoDialog",Context.MODE_PRIVATE).getBoolean("show",true);
    }

    public void setUserLocation(float latitude, float longitude){
        SharedPreferences.Editor editor = context.getSharedPreferences("location", Context.MODE_PRIVATE).edit();
        editor.putFloat("latitude",latitude);
        editor.putFloat("longitude",longitude);
        editor.commit();
    }

    public double getUserLatitude(){
        double longitude = context.getSharedPreferences("location",Context.MODE_PRIVATE).getFloat("latitude",0);
//        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
//        locationManager.get
        return longitude;
    }
    public double getUserLongitude(){
        double latitude = context.getSharedPreferences("location",Context.MODE_PRIVATE).getFloat("longitude",0);
        return latitude;
    }

    public void setShowDialog(boolean isActive){
        SharedPreferences.Editor editor = context.getSharedPreferences("ShowDialog", Context.MODE_PRIVATE).edit();
        editor.putBoolean("Show",isActive);
        editor.apply();
    }

    public boolean getShowDialog(){
//        SharedPreferences.Editor editor = context.getSharedPreferences("isActive", Context.MODE_PRIVATE).edit();
        return context.getSharedPreferences("ShowDialog",Context.MODE_PRIVATE).getBoolean("Show", true);
    }

}
