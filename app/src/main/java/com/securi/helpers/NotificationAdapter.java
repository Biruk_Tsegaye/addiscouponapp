package com.securi.helpers;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.securi.addiscpn.HomePage;
import com.securi.addiscpn.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michael on 8/18/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder>{
    TextView title, message,noNewNotifications;
    List<NotificationModel> modelList;
    ImageView notificationIcon;
    Button continueButton;
    Context context;
    View view;

    public NotificationAdapter(Context context, TextView noNewNotifications){
        modelList = new ArrayList<>();
        this.context = context;
        this.noNewNotifications = noNewNotifications;
        List<NotificationModel> notifList = NotificationModel.find(NotificationModel.class, "is_seen=?", "0");
        if (notifList != null) {
            for (int i = 0; i < notifList.size(); i++) {

                modelList.add(new NotificationModel(notifList.get(i).getnotif_Id(),notifList.get(i).getnotifType(),
                        (String) notifList.get(i).gettitle(), notifList.get(i).getmessage(), true));
                //notifList.get(i).setisSeen(true);
                notifList.get(i).setisSeen(true);
                notifList.get(i).save();
                }
        }

    }
    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list_item,parent,false);
        title = (TextView) view.findViewById(R.id.title);
        message = (TextView) view.findViewById(R.id.message);
        notificationIcon = (ImageView) view.findViewById(R.id.icon);
        continueButton = (Button) view.findViewById(R.id.continue_button);
        return new NotificationViewHolder(view);

    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, final int position) {
        final NotificationModel currentModel = modelList.get(position);

        title.setText(currentModel.gettitle());
        message.setText(currentModel.getmessage());
        String notificationType = modelList.get(position).getnotifType();


        if(notificationType.equalsIgnoreCase(NotificationModel.TYPE_UPDATE)){
            notificationIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_system_update_black_24dp));
            continueButton.setText("Update");
            continueButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("market://details?id=com.securi.addiscpn&hl=en"));
                    context.startActivity(intent);
                }
            });

        }else if(notificationType.equalsIgnoreCase(NotificationModel.TYPE_SPECIAL_DEAL)){
            notificationIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_stars_black_24dp));
            continueButton.setText("Open");
            continueButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, HomePage.class);
                    intent.putExtra("load_tab",4);
                    context.startActivity(intent);
                }
            });

        }else if(notificationType.equalsIgnoreCase(NotificationModel.TYPE_GENERIC)){
            notificationIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_info_outline_black_24dp));
            continueButton.setText("Read More");
            continueButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.AlertDialogCustom));
                    builder.setMessage(currentModel.getmessage())
                            .setTitle(currentModel.gettitle())
                            .setIcon(context.getResources().getDrawable(R.drawable.ic_info_outline_black_24dp))
                            .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .create();

                    builder.show();
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            modelList.remove(position);
                            notifyDataSetChanged();
                        }
                    });
                }
            });
        }else {
            notificationIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_info_outline_black_24dp));
            continueButton.setText("Proceed");
            continueButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, HomePage.class);
                    context.startActivity(intent);

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if(modelList.size() == 0){
            noNewNotifications.setVisibility(View.VISIBLE);
        }else{
            noNewNotifications.setVisibility(View.INVISIBLE);
        }
        return modelList.size();
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder{

        public NotificationViewHolder(View itemView) {
            super(itemView);
        }
    }

    public void removeNotification(int position){
        NotificationModel model = modelList.get(position);

        model.setisSeen(true);
        this.modelList.remove(position);
        notifyDataSetChanged();
    }

}
