package com.securi.helpers;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.securi.addiscpn.AppController;
import com.securi.addiscpn.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Zelalem G. on 7/26/2016.
 */

public class GridAdapter extends BaseAdapter {
    Activity context;
    int [] ourPhotos = {R.drawable.sample};
    JSONArray data;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    GridButtonClicked listener;
    int type;
    public static final int
            DEFAULT_LAYOUT = 0
            ,WIDE_LAYOUT = 1;


    String itemName,retailerName;
    SharedPrefs prefs;


    public GridAdapter(String layoutType,Activity cnxt, JSONArray webResponse, GridButtonClicked listener,int type){
        context = cnxt;
        this.listener = listener;
        data = webResponse;
        this.type = type;

        if(layoutType!=null && layoutType.equals("wide")){
            this.type = WIDE_LAYOUT;
        }else{
            this.type = DEFAULT_LAYOUT;
        }
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public int getCount() {
        return data.length();
    }

    @Override
    public Object getItem(int position) {
        try {
            return data.getJSONObject(position);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        try {
            return Long.valueOf(data.getJSONObject(position).getString("itemId"));
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Log.i("CURRENT POSITION IS: ", String.valueOf(position));
        LayoutInflater inflater = context.getLayoutInflater();
        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        View myView = null;

        if(type == DEFAULT_LAYOUT){
            myView = inflater.inflate(R.layout.style_grid_items, null);
        }else if(type == WIDE_LAYOUT){
            myView = inflater.inflate(R.layout.landscape_list_item, null);
        }

        prefs  = new SharedPrefs(this.context);

        TextView title = (TextView) myView.findViewById(R.id.txt_save);
        TextView tv_itemName = (TextView) myView.findViewById(R.id.txt_itemName);
        TextView bizName = (TextView) myView.findViewById(R.id.txt_retailorName);

        final TextView txtDetails = (TextView) myView.findViewById(R.id.txt_details);
        txtDetails.setTag(position);

        if(type!=WIDE_LAYOUT){
            txtDetails.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));

            txtDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int _position = (Integer)v.getTag();
                    GridButtonClicked cick =  listener;
                    cick.onGridDetailButtonClicked(_position);
                }
            });
        }

        final TextView txtAdd =(TextView) myView.findViewById(R.id.txt_add);
        txtAdd.setTag(position);
        txtAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int position = (Integer)v.getTag();
                    int itemId = data.getJSONObject(position).getInt("itemId");
                    listener.onGridAddButtonClicked(position, itemId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        //ImageView imgItem =(ImageView) myView.findViewById(R.id.img_item);
        ImageView thumbNail = (ImageView) myView.findViewById(R.id.img_item);
        ImageView imgPicked = (ImageView) myView.findViewById(R.id.img_picked);

        //imgAdded.setVisibility(View.INVISIBLE);

        try {

            if (prefs.getLanguage().equals("am")){
                itemName = "itemNameAmharic";
                retailerName = "retailerNameAmharic";
            }else{
                itemName = "itemName";
                retailerName = "retailerName";
            }

            CpnUtils utils = new CpnUtils();
            JSONObject item = data.getJSONObject(position);
            double discount = utils.RoundMyDoubleDigits(item.getDouble("discount"));
            if (item.getDouble("discount") == 0)
                title.setText(item.getString("nonDiscountDesc"));
            else
                title.setText(String.format("%,.2f",discount)+" "+ context.getResources().getString(R.string.gridadapter_birr));
//                                +  );
            
            if(item.getInt("itemQuantity") <= 0)
                title.setText("SOLDOUT!");
            bizName.setText(item.getJSONObject("retailer").getString(retailerName));
            tv_itemName.setText(item.getString(itemName));

//            thumbNail.setImageUrl(Constants.IMAGE_URL + item.getString("picName"), imageLoader);
            Picasso.with(context)
                    .load(Constants.IMAGE_URL + item.getString("picName"))
                    .placeholder(R.mipmap.background)
                    .into(thumbNail);

            // -- FIXING UI INCONSISTANCE -- \\
            CorrectTextFlow(tv_itemName);
            CorrectTextFlow(bizName);

            //--- MARKS ITEM AS PICKED ---\\
            if (item.getBoolean("picked")){
                if(imgPicked.getVisibility() == View.INVISIBLE){

                    imgPicked.setVisibility(View.VISIBLE);
//                    YoYo.with(Techniques.Tada).playOn(imgPicked);
                    txtAdd.setText(R.string.style_grid_items_txt_leave);
                    txtAdd.setBackgroundColor(Color.RED);

                }
            }else{
                imgPicked.setVisibility(View.INVISIBLE);
                txtAdd.setText(R.string.style_grid_items_txt_add);
                txtAdd.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return myView;
    }

    public void setType(int type) {
        this.type = type;
    }

    public interface GridButtonClicked {
        public int onGridDetailButtonClicked(int position);
        public int onGridAddButtonClicked(int position, int itemId);
    }

    private void CorrectTextFlow(final TextView tv){
        tv.post(new Runnable() {
            @Override
            public void run() {
                tv.setMaxHeight(2);
                tv.setMinLines(2);
                tv.setMinHeight(2);
                tv.setMaxLines(2);

            }
        });
    }

}
