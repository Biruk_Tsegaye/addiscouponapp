package com.securi.helpers;

import com.orm.SugarRecord;

/**
 * Created by Michael on 7/26/2017.
 */

public class CategoryModel extends SugarRecord {
    private String name,description;
    private int icon;
    private long id;

    public CategoryModel(){

    }
    public CategoryModel(long id,String categoryName,String description, int icon){
        this.name = categoryName;
        this.description = description;
        this.icon = icon;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
