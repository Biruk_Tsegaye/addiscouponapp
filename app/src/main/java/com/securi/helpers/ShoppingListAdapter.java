package com.securi.helpers;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.securi.addiscpn.AppController;
import com.securi.addiscpn.BusinessDetails;
import com.securi.addiscpn.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Level1 on 7/26/2016.
 */
public class ShoppingListAdapter extends BaseAdapter {
    Activity context;
    ImageLoader imageLoader;
    List<ClaimsModel> modelList;
    String headers;
    boolean highlighted = false;
    String retailerName;
    SharedPrefs prefs;
    public ShoppingListAdapter(Activity context, List<ClaimsModel> _modelList) {
        this.context = context;
        modelList = _modelList;
        headers = "";
    }

    @Override
    public int getCount() {
        return modelList.size();
    }

    @Override
    public Object getItem(int position) {
        return modelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0; //modelList.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View myView = inflater.inflate(R.layout.list_item_style, null);
        final ClaimsModel model = modelList.get(position);

        try {
            if(!headers.equals(model.GetRetailorName())){
                headers = model.GetRetailorName();
                if(!highlighted) {
                    highlighted = true;
                    myView.setBackgroundColor(Color.LTGRAY);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        prefs = new SharedPrefs(this.context);
        if(prefs.getLanguage().equals("am")){
            retailerName = "retailerNameAmharic";
        }else{
            retailerName = "retailerName";
        }

        TextView txtBizName = (TextView) myView.findViewById(R.id.txt_busines);
        TextView txtItemName = (TextView) myView.findViewById(R.id.txt_ItemName);
        TextView txtOrigPrice = (TextView) myView.findViewById(R.id.txt_originalPrice);
        TextView txtCpnPrice = (TextView) myView.findViewById(R.id.txt_cpnPrice);

        ImageButton delete = (ImageButton) myView.findViewById(R.id.delete_item);
        delete.setVisibility(View.GONE);

        if(imageLoader == null){
            imageLoader = AppController.getInstance().getImageLoader();
        }

        ImageView thumbNail = (ImageView) myView.findViewById(R.id.img_ItemPic);
        String picUrl  = Constants.IMAGE_URL + model.getPicName();
        Glide.with(context).load(picUrl).into(thumbNail);


        txtBizName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    CpnUtils utils = new CpnUtils();
                    JSONObject object = new JSONObject();
                    Intent businessDetail = new Intent(context, BusinessDetails.class);
                    try {
                        object.put(retailerName, model.GetRetailorName());
                        object.put("phone", model.getPhone());
                        object.put("latitude", model.getLatitude());
                        object.put("longtude", model.getLongitude());
                        object.put("locationDetails", model.getLocation());
                        object.put("$id", 0);
                        object.put("city", model.getCity());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                /*RetailerModel ratailer = new RetailerModel(model.GetRetailorName(), model.getCategory(),
                                        model.getLatitude(), model.getLongitude(), model.getLocation());*/
                    businessDetail.putExtra("retailerJson", object.toString());
                    context.startActivity(businessDetail);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        txtBizName.setVisibility(View.VISIBLE);
        txtBizName.setText(model.GetRetailorName());
        txtItemName.setText(model.getItemName());
        txtOrigPrice.setText(context.getString(R.string.j_detail_ordinary)+ String.valueOf(model.getOriginalPrice()));
        txtCpnPrice.setText(context.getString(R.string.j_detail_addis) + String.valueOf(model.GetCouponPrice()));

        txtOrigPrice.setPaintFlags(txtOrigPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        return myView;
    }
}
