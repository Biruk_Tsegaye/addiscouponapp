package com.securi.helpers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.securi.addiscpn.AppController;
import com.securi.addiscpn.Home;
import com.securi.addiscpn.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.securi.helpers.Constants.BASE_URL;
import static com.securi.helpers.Constants.GET_ITEM_CAT_BY_BUSI_CAT;

/**
 * Created by Michael on 8/7/2017.
 */

public class SubcategoriesAdapter extends RecyclerView.Adapter<SubcategoriesAdapter.SubcategoriesViewHolder>{
    private List<SubcategoryModel> subcategories;

    Context context;
    TextView title,description;
    NetworkImageView subcategoryPicture;
    String CatagoryName, CatagoryCity;
    JsonArrayRequest request;
    ImageLoader imageLoader;
    ProgressDialog progressDialog;
    Typeface cursiveFont, casualFont;
    String retailerName;
    SharedPrefs prefs;
    public SubcategoriesAdapter(final Context context){

        subcategories = new ArrayList<>();
        this.context = context;
        progressDialog = new ProgressDialog(context, R.style.MyTheme);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        progressDialog.show();

        prefs = new SharedPrefs(this.context);
        if(prefs.getLanguage().equals("am")){
            retailerName = "retailerNameAmharic";

        }else{
            retailerName = "retailerName";

        }

        String url = Constants.BASE_URL + "/Api/RetailerByCategory_v2/1";
        request = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
            JSONObject currentObject;
                try {
                    for(int i =0; i < response.length(); i++){
                        currentObject = response.getJSONObject(i);
                        subcategories.add(new SubcategoryModel(currentObject.getLong("retailerId"),currentObject.getString(retailerName),currentObject.getString("descripiton"),currentObject.getInt("categoryId")));
                        notifyDataSetChanged();
                    }
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Couldn't load data",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });

        AppController.getInstance().addToRequestQueue(request, "Tag");
        AssetManager assetManager = context.getApplicationContext().getAssets();
        cursiveFont = Typeface.createFromAsset(assetManager,String.format(Locale.US, "fonts/%s", "dancing_script.ttf"));
        casualFont = Typeface.createFromAsset(assetManager,String.format(Locale.US, "fonts/%s", "coming-soon.regular.ttf"));


    }

    @Override
    public SubcategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.subcategory_list_item,parent,false);
        title = (TextView) itemView.findViewById(R.id.category_name);
//        subcategoryPicture = (NetworkImageView) itemView.findViewById(R.id.category_icon);
        title.setTypeface(casualFont);

        return new SubcategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SubcategoriesViewHolder holder, final int position) {
        title.setText(subcategories.get(position).getSubcategoryName());
        holder.getItemView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPrefs prefs = new SharedPrefs(context);
                Intent homeIntent = new Intent(context.getApplicationContext(), Home.class);
                long id = subcategories.get(position).getId();
                int catID = subcategories.get(position).getCategoryId();

                String url = String.valueOf(Constants.GET_ALL_ITEMS_BY_BUSINESS + id + "/");
                String tabs_url = String.valueOf(BASE_URL + GET_ITEM_CAT_BY_BUSI_CAT + catID);

                homeIntent.putExtra("HOME_URL",url);
                homeIntent.putExtra("TAB_URL",tabs_url);
                homeIntent.putExtra("CAT_ID",((int) id));
                homeIntent.putExtra("Category",subcategories.get(position).getSubcategoryName());
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(homeIntent);

            }
        });
//        String imagesUrl = "";
//        subcategoryPicture.setImageUrl(imagesUrl, imageLoader);

            if (position == getItemCount() + 1) {
                progressDialog.dismiss();
            }
        }

    @Override
    public int getItemCount() {
        return subcategories.size();
    }

    class SubcategoriesViewHolder extends RecyclerView.ViewHolder{
        View itemView;
        public SubcategoriesViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
        }

        public View getItemView() {
            return itemView;
        }
    }



}
