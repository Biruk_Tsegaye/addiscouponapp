package com.securi.helpers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.securi.addiscpn.AppController;
import com.securi.addiscpn.Home;
import com.securi.addiscpn.MapActivity;
import com.securi.addiscpn.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.securi.helpers.Constants.BASE_URL;
import static com.securi.helpers.Constants.GET_ITEM_CAT_BY_BUSI_CAT;

/**
 * Created by Michael on 7/25/2017.
 */

public class RetailersAdapter extends RecyclerView.Adapter<RetailersAdapter.RetailersViewHolder>{
    Context context;
    TextView tv_retailerName,distance;
    ImageView retailerLogo;
    ImageButton showOnMapButton;
    List<RetailerModel> retailerModelList;
    JsonArrayRequest request;
    URL url;
    ImageLoader imageLoader;
    ProgressDialog progressDialog;
    String type;
    AsyncTask<Void,Void,Location> locationAsyncTask;
    LocationHelper locationHelper;
    String retailerName;
    SharedPrefs prefs;


    public RetailersAdapter(Context con, String type){
        this.context = con;
        retailerModelList = new ArrayList<RetailerModel>();
        this.progressDialog = new ProgressDialog(context,R.style.MyTheme);
        this.progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        this.type = type;
        progressDialog.show();
        locationHelper = new LocationHelper(con);
        loadItemsFromWeb();
//        progressDialog.dismiss();
        setHasStableIds(true);


    }

    @Override
    public RetailersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.retailers_list_item,parent,false);
        tv_retailerName = (TextView) itemView.findViewById(R.id.retailer_name);
        distance = (TextView) itemView.findViewById(R.id.distance);
        retailerLogo = (ImageView) itemView.findViewById(R.id.retailer_logo);
        showOnMapButton = (ImageButton) itemView.findViewById(R.id.show_on_map);
        return new RetailersViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RetailersViewHolder holder, final int position) {
        final RetailerModel model = retailerModelList.get(position);
        tv_retailerName = (TextView) holder.getItemView().findViewById(R.id.retailer_name);
        distance = (TextView) holder.getItemView().findViewById(R.id.distance);

        double d = 0;
        try {
            d = DistanceHelper.getDistanceFromLatLngInKm(model.getLatitude(),model.getLongitude(),locationHelper.getUserLatitude(),locationHelper.getUserLongitude());
            distance.setText(String.format("%,.2fkm away",(d/1000)));

        } catch (Exception e) {
            e.printStackTrace();
        }
        tv_retailerName.setText(retailerModelList.get(position).getName());
        View itemView = holder.getItemView();
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homeIntent = new Intent(context,Home.class);
                RetailerModel business = retailerModelList.get(position);

                String url = Constants.GET_ALL_ITEMS_BY_BUSINESS + business.getId() +"/";
                String tabs_url = String.valueOf(BASE_URL+ GET_ITEM_CAT_BY_BUSI_CAT + business.getCategoryId());

                if(!business.getRetailerCategory().equalsIgnoreCase("Shopping")){
                    homeIntent.putExtra("LayoutType","wide");
                }

                homeIntent.putExtra("HOME_URL",url);
                homeIntent.putExtra("TAB_URL",tabs_url);
                homeIntent.putExtra("CAT_ID", ((int) business.getId()));
                homeIntent.putExtra("Category",retailerModelList.get(position).getName());

                context.startActivity(homeIntent);


//                Intent intent = new Intent(context, BusinessDetails.class);
//                intent.putExtra("retailerModel",retailerModelList.get(position));
//                context.startActivity(intent);
            }
        });

        showOnMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//            Intent googleMapsIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:" + model.getLatitude() + "," + model.getLongitude() ));
//            if(googleMapsIntent.resolveActivity(context.getPackageManager()) != null){
//                context.startActivity(googleMapsIntent);
//            }else{
//                CpnUtils.ShowErrorDialog(context,"Error","You don't have google maps installed on your device");
//            }
                Intent intent = new Intent(context, MapActivity.class);
                intent.putExtra("latitude",model.getLatitude());
                intent.putExtra("longitude",model.getLongitude());
                intent.putExtra("name",model.getName());
                context.startActivity(intent);
            }
        });

        String category = model.getRetailerCategory().toLowerCase().trim();
        int logo;

        switch (category) {
            case ("shopping"):
                logo = R.mipmap.supermarket_icon;
                break;
            case ("supermarket"):
                logo = R.mipmap.supermarket_icon;
                break;
            case ("restaurants"):
                logo = R.mipmap.restaurant;
                break;
            case ("spas"):
                logo = R.mipmap.spa_icon;
                break;
            case ("travel and hotels"):
                logo = R.mipmap.travel;
                break;
            case ("events"):
                logo = R.mipmap.event;
                break;
            case ("getaways"):
                logo = R.mipmap.getaways;
                break;
            case("fitness"):
                logo = R.mipmap.gym;
                break;
            default:
                logo = R.mipmap.others1;
                break;
        }

        retailerLogo.setImageDrawable(context.getResources().getDrawable(logo));

    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }

    @Override
    public long getItemId(int position) {
        return position;
//        return m.get(position).getId();
    }


    public void loadItemsFromWeb(){
        String url = null;
        prefs = new SharedPrefs(this.context);

        if(prefs.getLanguage().equals("am")){
            retailerName = "retailerNameAmharic";
        }else{
            retailerName = "retailerName";
        }


        if(type.equalsIgnoreCase("all")){
            url = BASE_URL+ Constants.GET_ALL_RETAILERS;

        }else {

            Location userLocation = null;
                try {
                    userLocation = locationHelper.getUserLocation();
                    url = BASE_URL+ Constants.GET_NEARBY_BUSINESSES + userLocation.getLatitude() + "/" + userLocation.getLongitude() + "/";

                } catch (Exception e) {
//                    Toast.makeText(context,"Couldnt get user location" + e.getMessage(),Toast.LENGTH_LONG).show();
                }


//            Toast.makeText(context,url,Toast.LENGTH_LONG).show();

        }

//        Log.d("retailers_url",url);

        request = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                JSONArray itemsJsonArray = response;
                try {
                    retailerModelList = new ArrayList<>();
                    for(int i=0; i < response.length();i++){
                        JSONObject obj = response.getJSONObject(i);
                        Log.d("VOLLEYY",obj.toString());
                        RetailerModel currentModel = new RetailerModel();

                        currentModel.setId(Long.valueOf(obj.getString("retailerId")));
                        currentModel.setName(obj.getString(retailerName).replace('_',' '));
                        currentModel.setLatitude(obj.getDouble("latitude"));
                        currentModel.setPicName(obj.getString("picNameRetailer"));
                        currentModel.setLongitude(obj.getDouble("longtude"));
                        currentModel.setLocationDetails(obj.getString("locationDetails"));
                        currentModel.setPhoneNumber(obj.getString("headContactPhone"));
                        currentModel.setCity(obj.getString("city"));
                        currentModel.setEmail(obj.getString("headContactEmail"));
                        currentModel.setCategoryId(Long.valueOf(obj.getString("categoryId")));
                        String category = null;

                        try{
                            category = obj.getString("categoryName");
                            currentModel.setRetailerCategory(category);

                            //                        Log.d("CURRENT_MODEL",obj.toString());
                            if(!retailerModelList.contains(currentModel))
                                retailerModelList.add(currentModel);

                            if(currentModel.getPicName() != null)
                               // Glide.with(context).load(Constants.BASE_URL_TEST + Constants.RETAILER_IMAGE + currentModel.getPicName()+".jpg");

                            notifyDataSetChanged();
                        }catch (JSONException ex){
                            Toast.makeText(context,"json expetion" +ex.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    }
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    Toast.makeText(context,"json exception" + e.getMessage(),Toast.LENGTH_LONG).show();
                    RetailersAdapter.this.progressDialog.dismiss();
                    progressDialog.dismiss();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(context,"volley error" +),Toast.LENGTH_LONG).show();
                error.printStackTrace();
                RetailersAdapter.this.progressDialog.dismiss();
                progressDialog.dismiss();

            }
        });

        AppController.getInstance().addToRequestQueue(request, "Tag");
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return retailerModelList.size();
    }

    public class RetailersViewHolder extends RecyclerView.ViewHolder{
        View itemView;
        public RetailersViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
        }
        public View getItemView(){
            return itemView;
        }
    }

    public void setRetailerModelList(List<RetailerModel> retailerModelList) {
        this.retailerModelList = retailerModelList;
    }

    public Location getUserLocation(){
        if(locationAsyncTask!=null){
            locationAsyncTask.cancel(true);
        }
        Location location;
         locationAsyncTask = new AsyncTask<Void, Void, Location>() {
            @Override
            protected Location doInBackground(Void... params) {
                LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                String provider = locationManager.getBestProvider(new Criteria(),false);
                Location location = locationManager.getLastKnownLocation(provider);
                return location;
            }

             @Override
             protected void onPostExecute(Location location) {
                 super.onPostExecute(location);
             }
         };


        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
