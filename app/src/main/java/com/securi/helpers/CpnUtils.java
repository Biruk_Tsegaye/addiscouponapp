package com.securi.helpers;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.widget.Toast;

import com.securi.addiscpn.R;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Level1 on 7/26/2016.
 */
public class CpnUtils {

    //----- CHECK IF CONNECTION IS AVAILABLE -----
    public static Boolean checkConnection(Context context){
        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            return false;
        }
        return true;
    }

    public static Double RoundMyDoubleDigits(double longDigit){
        DecimalFormat df = new DecimalFormat("#.##");
        return Double.valueOf(df.format(longDigit));
    }

    //----- ERROR DIALOG VIEWER -----
    public static void ShowErrorDialog(Context context, String title, String message) {
        try {
            AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.AlertDialogCustom));

            alertDialog2.setTitle(title);
            alertDialog2.setMessage(message);
            //alertDialog2.setIcon(R.drawable.ic_launcher);

            alertDialog2.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });

            alertDialog2.show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    public static String CorrectDateTimeFormate(String dt){
        //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat format = new SimpleDateFormat("ddd-MMM-ddHH:mm:ss'T'yyyy");
        SimpleDateFormat serverFormat = new SimpleDateFormat("MM-dd-yyyy");

        try {
            Date parsed = format.parse(dt);
            String corrected = serverFormat.format(parsed);
            Log.i("DATE DATE DATE", corrected.toString());
            return corrected;
        } catch (Exception e) {
            e.printStackTrace();
            return "Date Not Found!";
        }
    }

    public static String ReformateLocalDate(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        int day = cal.get(Calendar.DATE);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);
        return String.valueOf(day) + "/" + String.valueOf(month) + "/" + String.valueOf(year);
    }
}
