package com.securi.helpers;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.securi.addiscpn.R;

import java.util.List;

/**
 * Created by Level1 on 7/26/2016.
 */

public class TicketListAdapter extends BaseAdapter {
    Activity context;
    CouponClicked listner;
    List<ClaimsModel> modelList;
    ClaimsModel model;

    LayoutInflater inflater;

    public TicketListAdapter(Activity context, List<ClaimsModel> _modelList) {
        this.context = context;
        modelList = _modelList;
        listner = (CouponClicked) context;
    }

    @Override
    public int getCount() {
        return modelList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0; //modelList.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            LayoutInflater inflater = context.getLayoutInflater();
            View myView = inflater.inflate(R.layout.list_tickets_style, null);
            model = modelList.get(position);

            View v = convertView;

            TextView txtItemName = (TextView) myView.findViewById(R.id.txt_ItemName);
            TextView txtOrigPrice = (TextView) myView.findViewById(R.id.txt_originalPrice);
            TextView txtCpnPrice = (TextView) myView.findViewById(R.id.txt_cpnPrice);
            ImageView imgitemPic = (ImageView) myView.findViewById(R.id.img_ItemPic);
            ImageView deleteItem = (ImageView) myView.findViewById(R.id.delete_item);

            deleteItem.setTag(position);
            deleteItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ClaimsModel deleteModel = modelList.get(position);

                    int ticketCode = deleteModel.getClaimCode();

                    model.deleteAll(ClaimsModel.class, "claim_Code = ?", String.valueOf(ticketCode));

                    modelList.remove(position);
                    notifyDataSetChanged();

                }
            });

            LinearLayout llyCouponItem = (LinearLayout) myView.findViewById(R.id.lly_couponItem);
            llyCouponItem.setTag(position);
            llyCouponItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listner.couponClicked(position);
                }
            });


            txtItemName.setText(context.getString(R.string.ticketno) + this.model.getClaimCode());
            txtOrigPrice.setText(context.getString(R.string.claimedon) + CpnUtils.ReformateLocalDate(this.model.getClaimDate()));
            String previews = this.model.getItemName();
            if(previews.length() > 55)
                previews = previews.substring(0, 53) + "...";
            txtCpnPrice.setText(previews);

            return myView;

        }catch (Exception e){
            return null;
        }
    }

    public interface CouponClicked {
        void couponClicked(int position);
    }

}
