package com.securi.helpers;

/*
 * Created by Zelalem G. on 7/29/2016.
 * 0911492573
 */
public class Constants {

    /***** Cloud api *****/
  public static final String BASE_URL= "http://addiscoupon.azurewebsites.net/";

    /***** Server api *****/
//    public static final String BASE_URL="http://coupontest-001-site1.mysitepanel.net/";
//    public static final String BASE_URL = "http://192.168.0.30/";

    /***** TestServer api *****/
//    public static final String BASE_URL_TEST ="http://couapppontest-001-site1.mysitepanel.net/";
//    public static final String BASE_URL_TEST ="http://coupon-001-site1.mysitepanel.net/";
//    public static final String BASE_URL_TEST ="http://192.168.0.20/";
//   public static final String BASE_URL = "http://192.168.0.30/";

    public static final String IMAGE_URL = BASE_URL + "Images/ItemsPic/";
    public static final String All_RETAILERS_IMAGE = BASE_URL + "Images/Retailers/";
    public static final String ITEM_CATEGORY_IMAGE = BASE_URL + "Images/ItemCategory/";
    public static final String RETAILER_IMAGE = BASE_URL + "Images/Business/";
    public static final String TREDING_ITEMS_URL = "Api/Item/GetHotItemList_v2/";
    public static final String SPECIAL_OFFERS_URL = "Api/Item/GetSpecialOffers_v2/";
    public static final String RETAILERS_URL = "Api/RetailerByCategory_v2/";
    public static final String ITEM_BY_CATEGORY = "Api/Item/ItemByCategory_v2/";
    public static final String ITEM_BY_BUSINESS = "Api/Item/ItemByBusiness_v2/";
    public static final String ITEM_CATEGORY_LIST_BY_BUSINESS = "Api/ItemCategoryListByBusinessCat_V2/";

    public static final String ITEM_BY_CATEGORY_BUSINESS = "Api/Item/ItemByCategoryBusiness_v2/";

    public static final String GET_ALL_ITEMS_BY_BUSINESS = "Api/Item/ItemByBusinesOnly_v2/";

    public static final String GET_ITEM_CAT_BY_BUSI_CAT = "Api/ItemCategoryListByBusinessCat_V2/";

    public static final String GET_ALL_RETAILERS = "Api/GetAllRetailer_v2/";

    public static final String BASE_URL_PROD ="http://192.168.0.103/";

    public static final String GET_NEARBY_BUSINESSES = "Api/Retailer/GetnearbylocationAll/";

    public static final String GET_ITEM_BY_BUSINESS_AND_ITEM_CAT = "Api/Item/ItemByBusinesandItemCat_v2/";

    public static final String SEARCH = "Api/Item/Search/";

    public static final double CURRENT_VERSION = 5;


}
