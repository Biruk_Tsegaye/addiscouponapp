package com.securi.helpers;

import com.orm.SugarRecord;

/**
 * Created by bk on 7/20/2017.
 */

public class NotificationModel extends SugarRecord {
    int notif_Id;
    String notif_Type, message;
    String title;
    boolean isSeen;

    public static String
            TYPE_UPDATE ="Update",
            TYPE_SPECIAL_DEAL ="SpeacialDeals",
            TYPE_GENERIC="Generic";

    public NotificationModel(){
        
    }

    public NotificationModel(int _notifId, String _notifType, String _title , String _message, boolean _isSeen){
        this.notif_Id = _notifId;
        this.notif_Type = _notifType;
        this.message = _message;
        this.title = _title;
        this.isSeen = _isSeen;
    }

    //getters and setters
    public int getnotif_Id(){
        return this.notif_Id;
    }

    public String getnotifType(){
        return this.notif_Type;
    }

    public String getmessage(){
        return this.message;
    }

    public CharSequence  gettitle(){
        return this.title;
    }
    public boolean getisSeen(){return this.isSeen;}


    public void setnotif_Id(int notif_id) {
        this.notif_Id = notif_id;
    }
    public void setnotif_Type(String notif_Type) {
        this.notif_Type = notif_Type;
    }
    public void setmessage(String message){this.message = message;}
    public void settitle(String title){this.title = title;}
    public void setisSeen(boolean isSeen){this.isSeen = isSeen;}


}
