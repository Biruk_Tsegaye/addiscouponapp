package com.securi.helpers;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.securi.addiscpn.R;

public class DialogActivity extends AppCompatActivity {
    CheckBox dialogCheckbox;
    SharedPrefs prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
        this.setFinishOnTouchOutside(true);

        prefs = new SharedPrefs(getApplicationContext());


        if (!prefs.getShowDialog()){
            finish();
        }

        TextView tv_title = (TextView) findViewById(R.id.dialog_title);
        TextView tv_msg = (TextView) findViewById(R.id.dialog_msg);
        Button bt_ok = (Button) findViewById(R.id.dialog_ok);
        dialogCheckbox = (CheckBox) findViewById(R.id.dialog_dont_show_me_again);

        ImageView im = (ImageView) findViewById(R.id.navigation_screenshot);
        if(prefs.getLanguage().equals("am")){
            im.setImageResource(R.mipmap.navigation_am);
        }else{
            im.setImageResource(R.mipmap.navigation_en);
        }

        setTitle("");

        tv_title.setText(getResources().getString(R.string.dialog_title));
        tv_msg.setText(getResources().getString(R.string.dialog_msg));


        dialogCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialogCheckbox.isChecked()){
                    prefs.setShowDialog(false);
//                    finish();
                }else{
                    prefs.setShowDialog(true);
                }

            }
        });

        bt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
