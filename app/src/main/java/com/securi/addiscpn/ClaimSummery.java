package com.securi.addiscpn;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.securi.helpers.ClaimBuffer;
import com.securi.helpers.ClaimsModel;
import com.securi.helpers.Constants;
import com.securi.helpers.DialogActivity;
import com.securi.helpers.ListAdapter;
import com.securi.helpers.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Level1 on 7/31/2016.
 */
public class ClaimSummery extends Activity implements ClaimBuffer.ClaimChangeListener {
    Button btnGetCoupon, btnGoBack;
    List<ClaimsModel> claimsModel;
    ListView lstClaims;
    JSONArray jsonArray;
    ListAdapter adapter;
    SharedPrefs prefs;
    String itemName,retailerName;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    //private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.claim_summery);

        prefs = new SharedPrefs(ClaimSummery.this);
        lstClaims = (ListView) findViewById(R.id.img_barcode);
        btnGetCoupon = (Button) findViewById(R.id.btn_getCoupon);
        btnGoBack = (Button) findViewById(R.id.btn_Exit);

        btnGetCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubmitListOfClaimsToServer();
            }
        });

        btnGoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();

            }
        });

        jsonArray = ClaimBuffer.getInstance().GetClaims(ClaimSummery.this);
        adapter = new ListAdapter(ClaimSummery.this, jsonArray);
        lstClaims.setAdapter(adapter);
        //client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        if(prefs.getLanguage().equals("am")){
            itemName = "itemNameAmharic";
            retailerName = "retailerNameAmharic";
        }else {
            itemName = "itemName";
            retailerName = "retailerName";
        }
        }

    private void SubmitListOfClaimsToServer() {
        JSONArray arr = null;
        try {
            JSONObject obj = new JSONObject();
            arr = new JSONArray();
            obj.put("itemId", 2);
            arr.put(obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String tag_json_obj = "json_obj_req";

        String url = Constants.BASE_URL + "Api/ClaimedCoupon/Add/" + prefs.getUserId();
        prefs = new SharedPrefs(getApplicationContext());


        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();


        JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.POST,
                url, jsonArray,
                new Response.Listener<JSONArray>() {


                    @Override
                    public void onResponse(JSONArray response) {
                        pDialog.dismiss();

                        //-------- STORE CLAIM TO DB ----------\\
                        try {
                            ClaimsModel model = new ClaimsModel();
                            for (int i = 0; i < response.length(); i++) {


                                JSONObject item = response.getJSONObject(i);

                                 model = new ClaimsModel(item.getJSONObject("item").getString(itemName),
                                        item.getJSONObject("item").getDouble("originalPrice"),
                                        item.getJSONObject("item").getDouble("discount"), item.getInt("claimCode"),
                                         item.getJSONObject("item").getJSONObject("retailer").getString(retailerName),
                                         false, item.getJSONObject("item").getString("picName"),
                                         item.getJSONObject("item").getJSONObject("retailer").getString("locationDetails"),
                                         item.getJSONObject("item").getJSONObject("retailer").getString("phone"),
                                         item.getJSONObject("item").getJSONObject("retailer").getString("locationDetails"),
                                         item.getJSONObject("item").getJSONObject("retailer").getString("city"),
                                         "",//item.getJSONObject("item").getJSONObject("retailer").getString("catagory"),
                                         item.getJSONObject("item").getJSONObject("retailer").getDouble("latitude"),
                                         item.getJSONObject("item").getJSONObject("retailer").getDouble("longtude"));
                                model.save();


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        /*try {
                            SharedPrefs prefs = new SharedPrefs(ClaimSummery.this);
                            prefs.setUserAsRegistered();
                            prefs.saveUserName(response.getJSONObject(0).getString("userName"));
                            startActivity(new Intent(ClaimSummery.this, Home.class));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }*/


//                        Toast.makeText(ClaimSummery.this, getResources().getString(R.string.request_submitted), Toast.LENGTH_SHORT).show();


                        ClaimBuffer.getInstance().ResetClaim(ClaimSummery.this);
                        //prefs.saveClaimedItems(null);
////

                        finish();
                        pDialog.hide();
                        if(prefs.getShowDialog()){
                            startActivity(new Intent(ClaimSummery.this, DialogActivity.class));
                        }

                        Intent returnIntent = new Intent();
                        setResult(Activity.RESULT_OK, returnIntent);



                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("WEB RESPONSE-QQQQQQ", "Error: " + error.toString());
                Toast.makeText(ClaimSummery.this, getResources().getString(R.string.couldnt_connect_to_sever), Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("userName", "man");
                params.put("phone", "09876565");
                params.put("userKey", "P@ssC0de123");
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }



    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        /*client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());*/
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
       /* AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();*/
    }

    @Override
    public void onCLaimChanged(JSONArray array) {
        if(ClaimBuffer.getInstance().GetClaims(ClaimSummery.this).length() <= 0)
            btnGetCoupon.setEnabled(false);
        else
            btnGetCoupon.setEnabled(true);

    }

}
