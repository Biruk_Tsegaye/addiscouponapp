package com.securi.addiscpn;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.securi.helpers.Constants;
import com.securi.helpers.SharedPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Register extends AppCompatActivity {
    Button btnRegister, btnReset;
    EditText edtName, edtPhone, edtRefId;
    TextView txtError, txtShowRef;
    String ReferalID, PromoCode;
    SharedPrefs prefs = new SharedPrefs(Register.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        RegisterControls();

        if (prefs != null){

            if (prefs.IsPromoCodeActive()) {
                edtRefId.setVisibility(View.VISIBLE);
                edtRefId.setHint(getResources().getString(R.string.enter_ur_promo_id));
            }
            if (prefs.IsReferalProgActive()) {
                edtRefId.setVisibility(View.VISIBLE);
                edtRefId.setHint(getResources().getString(R.string.enter_ur_ref_code));
            }

        }
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String phonenumber = edtPhone.getText().toString().trim();
                if(edtName.getText().toString().isEmpty()){
                    YoYo.with(Techniques.Shake).duration(700).playOn(edtName);
                    txtError.setText(getResources().getString(R.string.enter_ur_name));
                    txtError.setVisibility(View.VISIBLE);
                    return;
                }
                if(phonenumber.isEmpty()){// || edtPhone.getText().length() < 10
                    YoYo.with(Techniques.Shake).duration(700).playOn(edtPhone);
                    txtError.setText(getResources().getString(R.string.enter_ur_num));
                    txtError.setVisibility(View.VISIBLE);
                    return;
                }

                boolean condition1 = phonenumber.startsWith("+2519")&& phonenumber.length() == 13 && phonenumber.substring(1).matches("\\d{12}"); // +2519########
                boolean condition2 = phonenumber.startsWith("09")&& phonenumber.length() == 10 && phonenumber.matches("\\d{10}"); // 09########
                boolean condition3 = phonenumber.startsWith("2519")&& phonenumber.length() == 12 && phonenumber.matches("\\d{12}"); // 2519########
                boolean condition4 = phonenumber.startsWith("9")&& phonenumber.length() == 9 && phonenumber.matches("\\d{9}"); // 9########

                if(!condition1 && !condition2 && !condition3 && !condition4){
                    YoYo.with(Techniques.Shake).duration(700).playOn(edtPhone);
                    txtError.setText("Please Enter a valid phonenumber");
                    txtError.setVisibility(View.VISIBLE);
                    return;
                }

                if(condition2){
                    phonenumber = "+251" + phonenumber.substring(1);
                }else if(condition3){
                    phonenumber = "+" + phonenumber;
                }else if(condition4){
                    phonenumber = "+2519" + phonenumber.substring(1);
                }

                txtError.setVisibility(View.GONE);
                String name = "", phone = "",
                        trimedName = "";
                name = edtName.getText().toString();
                phone = phonenumber;
                trimedName = name.trim().replace(" ", "");
                name = trimedName.replace("\t", "");

                RegisterUserOnServer(name, phone);

                //LoadFromWeb();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtPhone.setText("");
                edtName.setText("");
            }
        });
    }

    private void RegisterUserOnServer(final String name, final String phone) {

        String ref  = edtRefId.getText().toString();

        if (prefs.IsPromoCodeActive()) {
            ReferalID = ref;
        }
        if (prefs.IsReferalProgActive()) {
            PromoCode = ref;
        }

        JSONObject obj = new JSONObject();

        try {
            obj.put("userName", name);
            obj.put("phone", phone);
            obj.put("userKey", "P@ssC0de123");
            obj.put("referralId", ReferalID);
            obj.put("promoCode", PromoCode);
            obj.put("IsActive", false);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String tag_json_obj = "json_obj_req";
        String url = Constants.BASE_URL + "Api/customer/AddCustomer";

        final ProgressDialog pDialog = new ProgressDialog(this, R.style.MyTheme);
        pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);

        pDialog.show();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, obj,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("WEB RESPONSE-QQQQQQ", response.toString());
                        pDialog.dismiss();
                        try {
                            prefs.setUserAsRegistered();
                            prefs.saveUserName(response.getString("userName"));
                            prefs.saveUserId(response.getInt("customerId"));

                            Intent i = new Intent(Register.this, Verification.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                            startActivity(i);
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("WEB RESPONSE-QQQQQQ", "Error: " + error.getMessage());
                Toast.makeText(Register.this, getResources().getString(R.string.couldnt_connect_to_sever), Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }
        })
        {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("userName", "man");
                params.put("phone", "09876565");
                params.put("userKey", "P@ssC0de123");
                params.put("referralId", ReferalID);
                params.put("promocode", PromoCode);
                 params.put("IsActive", String.valueOf(false));
                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private void RegisterControls() {
        btnRegister = (Button) findViewById(R.id.btn_Submit);
        btnReset = (Button) findViewById(R.id.btn_Reset);
        edtName = (EditText)findViewById(R.id.edt_Name);
        edtRefId = (EditText)findViewById(R.id.edt_RefId);
        edtPhone = (EditText)findViewById(R.id.edt_Phone);
        txtError = (TextView) findViewById(R.id.txt_Error);
        txtError.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {

        finish();
        System.exit(0);
    }


}
