package com.securi.addiscpn;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.securi.helpers.SharedPrefs;

/**
 * Created by Michael on 8/17/2017.
 */

public class HelpFragment extends Fragment {

    ViewPager pager;
    int layoutId;
    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(getArguments() != null) {
            layoutId = getArguments().getInt("layout_id");
        }

        View view = inflater.inflate(layoutId,container,false);


//        next.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//            if(pager != null){
//                if(pager.getCurrentItem() == 7){
//                    SharedPrefs prefs = new SharedPrefs(getContext());
//                    Intent intent;
//                    if(!prefs.getRegistrationState()){
//                        intent = new Intent(getContext(),Register.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    }else{
//                        intent = new Intent(getContext(),HomePage.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    }
//                    startActivity(intent);
//                }else{
//                    pager.setCurrentItem(pager.getCurrentItem()+1);
//                }
//            }
//            }
//        });

        return view;
    }


    public ViewPager getPager() {
        return pager;
    }

    public void setPager(ViewPager pager) {
        this.pager = pager;
    }
}
