package com.securi.addiscpn;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.securi.helpers.Constants;
import com.securi.helpers.CpnUtils;
import com.securi.helpers.SharedPrefs;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Level1 on 7/29/2016.
 */
public class Details extends Activity {
    Button btnAddItem, btnNoThanks;
    TextView txtOriginalPrice, txtCouponPrice, txtDescription,txt_ItemTitle,txtRetailer;
    SharedPrefs prefs;
    String itemName,retailerName;
    String itemDescription;

    String itemId = "0", position = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details);

        prefs = new SharedPrefs(getApplicationContext());

        txt_ItemTitle =(TextView) findViewById(R.id.txt_ItemTitle);
        txtDescription = (TextView) findViewById(R.id.txt_description);
        txtOriginalPrice = (TextView) findViewById(R.id.txt_orignalPrice);
        txtCouponPrice = (TextView) findViewById(R.id.txt_couponPrice);
        btnNoThanks = (Button) findViewById(R.id.btn_cancel);
        btnAddItem = (Button) findViewById(R.id.btn_addToList);
        txtRetailer = (TextView) findViewById(R.id.txt_retailer);

        ImageLoader imageLoader = AppController.getInstance().getImageLoader();
        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        ImageView imgNetWorkView = (ImageView) findViewById(R.id.edt_SearchItem);


        if(prefs.getLanguage().equals("am")){

            itemName = "itemNameAmharic";
            itemDescription  = "itemDescriptionAmharic";
            retailerName = "retailerNameAmharic";
        }else {

            itemName = "itemName";
            itemDescription = "itemDescription";
            retailerName = "retailerName";
        }


        final Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            try {
                position = String.valueOf(bundle.getInt("pos"));
                JSONObject item = new JSONObject(bundle.getString("data"));
                double cpnPrice = Double.valueOf(item.getString("originalPrice"))
                        - Double.valueOf(item.getString("discount"));
                cpnPrice = CpnUtils.RoundMyDoubleDigits(cpnPrice);
                itemId = item.getString("itemId");
                //position = bundle.getString("pos");
                txt_ItemTitle.setText(item.getString(itemName).trim());

                if (item.getDouble("discount") == 0){
                    txtOriginalPrice.setText("");
                    txtCouponPrice.setText(item.getString("nonDiscountDesc"));
                 } else{
                    txtOriginalPrice.setText(getResources().getString(R.string.j_detail_ordinary)+ item.getString("originalPrice"));
                    txtCouponPrice.setText(getResources().getString(R.string.j_detail_addis) + String.format("%,.2f",cpnPrice));
                }
                txtDescription.setText("");

                if(!item.getString(itemDescription).equals("null"))
                    txtDescription.setText(item.getString(itemDescription).trim());
                txtOriginalPrice.setPaintFlags(txtOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                Picasso.with(getApplicationContext())
                        .load(Constants.IMAGE_URL + item.getString("picName"))
                        .placeholder(R.mipmap.background)
                        .into(imgNetWorkView);

//                imgNetWorkView.setImageUrl(, imageLoader);

                btnAddItem.setText(getResources().getString(R.string.j_detail_add_item));
                btnAddItem.setTextColor(Color.WHITE);
                btnAddItem.setBackgroundDrawable(getResources().getDrawable(R.drawable.my_btn_style));
                if (item.getBoolean("picked")) {
                    btnAddItem.setText(getResources().getString(R.string.j_detail_remove_item));
                    btnAddItem.setBackgroundDrawable(getResources().getDrawable(R.drawable.red_btn_style));
                }
                final JSONObject retailerJsonObject = item.getJSONObject("retailer");
//
//                RetailerModel retailerModel = new RetailerModel();
//                retailerModel.setName(retailerJsonObject.getString("retailerName"));
//                retailerModel.setLocationDetails(retailerJsonObject.getString("locationDetails"));
//                retailerModel.setPhoneNumber(retailerJsonObject.getString("phone"));
//                retailerModel.setLatitude(retailerJsonObject.getDouble("latitude"));
//                retailerModel.setLongitude(retailerJsonObject.getDouble("longtude"));
//                retailerModel.setId(retailerJsonObject.getLong("$id"));
//                retailerModel.setCity(retailerJsonObject.getString("city"));
//                txtRetailer.setText(retailerJsonObject.getString("retailerName"));
                txtRetailer.setText(retailerJsonObject.getString(retailerName).replace('_',' ') + "...");
                txtRetailer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent businessDetails = new Intent(getApplicationContext(),BusinessDetails.class);
//                        businessDetails.putExtra("retailerModel",businessDetails);
                        businessDetails.putExtra("retailerJson",retailerJsonObject.toString());
                        startActivity(businessDetails);
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("ID",itemId);

                // --- added to detect clicked position --- \\
                returnIntent.putExtra("POS",position);
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }
        });

        btnNoThanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }
        });

    }
    //0984718692
}
