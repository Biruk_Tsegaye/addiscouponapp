package com.securi.addiscpn;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.securi.helpers.RetailersAdapter;

public class Retailers extends AppCompatActivity{
    RecyclerView retailersRecyclerView;
    Toolbar toolbar;
    SwipeRefreshLayout swipeRefreshLayout;
    RetailersAdapter adapter;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retailers);
        retailersRecyclerView = (RecyclerView) findViewById(R.id.retailers_recycler);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        adapter = new RetailersAdapter(this,"all");
        retailersRecyclerView.setAdapter(adapter);

//        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
//        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
//        int noOfColumns = (int)(dpWidth / 180);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
                swipeRefreshLayout.setRefreshing(false);

                adapter.loadItemsFromWeb();
                adapter.notifyDataSetChanged();
            }
        });

        retailersRecyclerView.setLayoutManager(new GridLayoutManager(this,1));
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.tab_businesses));




    }

    public void refresh(){
        adapter.loadItemsFromWeb();
        adapter.notifyDataSetChanged();

    }
}
