package com.securi.addiscpn;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.securi.helpers.Constants;
import com.securi.helpers.NotificationModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * Created by bk on 7/20/2017.
 */

public class NotificationService extends Service {

    public static final int notify = 5400000;  //interval between two services 2000000
    private Handler mHandler = new Handler();   //run on another Thread to avoid crash
    private Timer mTimer = null;    //timer handling

    public static NotificationManager notificationManager;

    String  notif_title;
    String notif_message;
    int notifId;
    String notif_Type;
    long LastVisit;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {

        if (mTimer != null) // Cancel if already existed
            mTimer.cancel();
        else
            mTimer = new Timer();   //recreate new
        mTimer.scheduleAtFixedRate(new TimeDisplay(), 0, notify);   //Schedule task
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    class TimeDisplay extends TimerTask {

        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {

                    String tag_string_req = "string_req";

                    String url = Constants.BASE_URL+ "Api/Notification_v2/0";
                    Log.e("Notification service", "requesting");
                    JsonArrayRequest strReq = new JsonArrayRequest(Request.Method.GET,
                            url, new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {

                            try {
                                for (int i = 0; i < response.length(); i++) {

                                    JSONObject jsonResponse = (JSONObject) response.get(i);
                                    //JSONObject jsonResponse = (JSONObject) response.get(response.length() - 1);
                                    if (jsonResponse != null) {

                                        String message = jsonResponse.getString("message");
                                        String title = jsonResponse.getString("title");
                                        int notif_id = jsonResponse.getInt("notificationId");
                                        String notifType = jsonResponse.getString("notificationType");

                                        notifId = notif_id;
                                        notif_title = title;
                                        notif_message = message;
                                        notif_Type = notifType;

                                        List<NotificationModel> notificationList = NotificationModel.findWithQuery(NotificationModel.class, "SELECT * FROM notification_model ORDER BY id DESC LIMIT 1 ");
                                        //for new users
                                        if (notificationList.isEmpty()){
                                            showNotification();
                                        }
                                        for (int j = 0; j < notificationList.size(); j++) {

                                            if (notificationList.get(j).getnotif_Id() < notifId) {

                                                showNotification();

                                                break;
                                            }

                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyLog.d("Error: " + error.getMessage());
                        }
                    });
                    // Adding request to request queue
                    AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(NotificationService.this);

                    Date myDate = new Date(preferences.getLong("time", 0));
                    Date now = new Date(System.currentTimeMillis());
                    // long now = date.getTime();

                    LastVisit = now.getTime() - myDate.getTime();
                    if (!isAppIsInBackground(getApplicationContext())){
                        LastVisit = 0;
                    }
                    //show notification if the app is not opened for 72hrs
                    if (LastVisit > 72000000){

                        Intent intent = new Intent(getApplicationContext(), HomePage.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0,
                                intent, FLAG_ACTIVITY_NEW_TASK);
                        Notification Visited = new Notification.Builder(NotificationService.this)

                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setContentTitle(getResources().getString(R.string.miss_u))
                                .setContentText(getResources().getString(R.string.ur_missing_out_special_deals))
                                .setContentIntent(contentIntent)
                                .setOngoing(false)
                                .build();

                        Date date = new Date(System.currentTimeMillis());

                        preferences.edit().putLong("time", date.getTime()).apply();

                        // Send the notification.
                        notificationManager.notify(R.string.local_service_started, Visited);
                    }

                }
            });
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();

        notificationManager.cancel(R.string.local_service_started);
        // notification stopped.
        Toast.makeText(this, R.string.local_service_stopped, Toast.LENGTH_SHORT).show();
    }

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;

                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    private void showNotification() {

        Intent intent = new Intent(this, NotificationActivity.class);
        intent.putExtra("Msg", notif_message);
        intent.putExtra("title", notif_title);
        intent.putExtra("notifcationId", notifId);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent contentIntent = PendingIntent.getActivity(this, notifId, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new Notification.Builder(this)

                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(notif_title)
                .setContentText(notif_message)
                .setContentIntent(contentIntent)
                .setOngoing(false)
                .setLights(Color.GREEN, 3000, 3000)
                .setDefaults(Notification.DEFAULT_SOUND)
                //.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .build();

        NotificationModel toDb = new NotificationModel(notifId, notif_Type, notif_title, notif_message, false);
        toDb.save();
        // Send the notification.
        notificationManager.notify(R.string.local_service_started, notification);

    }
}