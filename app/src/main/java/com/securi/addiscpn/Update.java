package com.securi.addiscpn;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.securi.helpers.Constants;
import com.securi.helpers.SharedPrefs;

public class Update extends AppCompatActivity {
    ImageView updateIcon;
    Button updateButton, LaterButton;
    SharedPrefs prefs = new SharedPrefs(Update.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        updateIcon = (ImageView) findViewById(R.id.update_icon);
        updateButton = (Button) findViewById(R.id.update_button);
        LaterButton = (Button)findViewById(R.id.btn_later);

        if (Constants.CURRENT_VERSION < prefs.getCurrentAppVersion()){

            LaterButton.setVisibility(View.VISIBLE);

        }
        LaterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!prefs.getUserName().isEmpty() && prefs.IsActive()) {
                    Intent goHome = new Intent(Update.this, HomePage.class);
                    startActivity(goHome);
                    finish();
                }
                else if (prefs.getUserName().isEmpty() || !prefs.IsActive()) {
                    Intent registration = new Intent(Update.this, Register.class);
                    startActivity(registration);
                    finish();
                }
            }
        });

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YoYo.with(Techniques.RotateOut).playOn(updateIcon);

                Intent playStoreIntent = new Intent(Intent.ACTION_VIEW);
                playStoreIntent.setAction(Intent.ACTION_VIEW);
                playStoreIntent.setData(Uri.parse("market://details?id=com.securi.addiscpn&hl=en"));
                startActivity(playStoreIntent);
            }
        });
    }

}
