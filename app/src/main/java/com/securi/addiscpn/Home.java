package com.securi.addiscpn;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.securi.helpers.CategoriesAdapter;
import com.securi.helpers.CategoryModel;
import com.securi.helpers.ClaimBuffer;
import com.securi.helpers.Constants;
import com.securi.helpers.NotificationModel;
import com.securi.helpers.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;



public class Home extends AppCompatActivity implements ClaimBuffer.ClaimChangeListener  {
    private MenuItem specialDealsMenuItem;
    private FloatingActionButton fab;
    ViewPager pager;
    TabLayout tabLayout;
    ItemsFragment fragment;
    ItemsFragment frag;
    TextView notifText, claimCountTv;
    int activeNotifs = 0;
    HomePage.HomePagerAdapter pagerAdapter;
    SuperMarketPagerAdapter SuperMpageAdapter;
    List<CategoryModel> categoryList = new ArrayList<>();
    String layoutType;
    SharedPrefs prefs;
    String itemTypeName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        appBarImage = (ImageView) findViewById(R.id.app_bar_image);
//        appBarImage.setImageDrawable(getApplicationContext().getResources().getDrawable(getIntent().getIntExtra("icon",R.mipmap.title_bar)));

        prefs = new SharedPrefs(this);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        String url = getIntent().getExtras().getString("URL");
        layoutType = getIntent().getExtras().getString("LayoutType");
        String category = getIntent().getExtras().getString("Category");

        Bundle args = new Bundle();
        args.putString("URL",url);




//        if(layoutType!=null && layoutType.equals("wide")){
//            args.putString("LayoutType","wide");
//        }
//
//        fragment = new ItemsFragment();
//        fragment.setArguments(args);
////
//        transaction.add(R.id.viewPager_items_fragment, fragment).commit();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String title = getIntent().getStringExtra(CategoriesAdapter.CATEGORY);
        getSupportActionBar().setTitle(title);


        SuperMpageAdapter = new SuperMarketPagerAdapter(getSupportFragmentManager());
        tabLayout = (TabLayout) findViewById(R.id.tabLayout_items_fragment);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        pager = (ViewPager) findViewById(R.id.viewPager_items_fragment);
        pager.setAdapter(SuperMpageAdapter);
        pager.setOffscreenPageLimit(5);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        tabLayout.setupWithViewPager(pager);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_home_black_24dp);
        Intent callingIntent = getIntent();
        int loadTab = callingIntent.getIntExtra("load_tab",0);
        pager.setCurrentItem(loadTab);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//        }
        super.onBackPressed();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);

        MenuItem itemNotif = menu.findItem(R.id.action_notification);
        MenuItemCompat.setActionView(itemNotif, R.layout.style_actionbar_badge);
        RelativeLayout notifCount = (RelativeLayout) MenuItemCompat.getActionView(itemNotif);

        notifText = (TextView) notifCount.findViewById(R.id.actionbar_notifcation_textview);

        ImageView notif_Image = (ImageView)notifCount.findViewById(R.id.notificationImage);
        notif_Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Home.this, NotificationActivity.class));
            }
        });

        List<NotificationModel> notifList = NotificationModel.find(NotificationModel.class, "is_seen=?", "0");

        if (!notifList.isEmpty()) {
            for (int i = 0; i < notifList.size(); i++) {

                activeNotifs = notifList.size();
            }
        }

        if(activeNotifs == 0 || notifList.isEmpty())
            notifText.setVisibility(View.INVISIBLE);
        else if(activeNotifs > 99){
            notifText.setText(String.valueOf(99));
        }
        else
            notifText.setVisibility(View.VISIBLE);


        notifText.setText(String.valueOf(activeNotifs));
        notifText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(Home.this, NotificationActivity.class));

            }
        });

        MenuItem itemCount = menu.findItem(R.id.action_redeem);
        MenuItemCompat.setActionView(itemCount, R.layout.style_actionbar_cart_count);
        FrameLayout claimCount = (FrameLayout) MenuItemCompat.getActionView(itemCount);

        ImageView imgActionCart = (ImageView) claimCount.findViewById(R.id.imagebuttonid);
        claimCountTv = (TextView) claimCount.findViewById(R.id.actionbar_notifcation_textview);
        final int prevClaims = ClaimBuffer.getInstance().GetClaims(Home.this).length();
        if (prevClaims <= 0)
            claimCountTv.setVisibility(View.INVISIBLE);
        else{
            claimCountTv.setText(String.valueOf(prevClaims));
            claimCountTv.setVisibility(View.VISIBLE);
        }

        imgActionCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                JSONArray claims = ClaimBuffer.getInstance().GetClaims(HomePage.this);
//                if(claims == null || claims.length() <= 0){
//                    CpnUtils.ShowErrorDialog(HomePage.this, "Warning!", " No item selected. " +
//                            "You need to select at least one item to get your coupon.");
//                }else{
//                    Intent summery = new Intent(HomePage.this, ClaimSummery.class);
//                    summery.putExtra("CLAIMS", claims.toString());
//                    startActivityForResult(summery, 2);
//                }
                ItemsFragment currentFrag;
                currentFrag = (ItemsFragment) SuperMpageAdapter.getItem(pager.getCurrentItem());
                currentFrag.ProcessClaims();
                //fragment.ProcessClaims();
            }
        });

        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_redeem) {
            if(pager.getCurrentItem()>0){
                frag = (ItemsFragment) pagerAdapter.getItem(pager.getCurrentItem());
                frag.ProcessClaims();
            }
            return true;

        } else if (id == R.id.action_search) {
            Intent searchIntent = new Intent(Home.this, Search.class);
            startActivity(searchIntent);
            return true;

        } else if(id== R.id.action_notification){
            Intent notificationIntent = new Intent(Home.this, NotificationActivity.class);
            startActivity(notificationIntent);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCLaimChanged(JSONArray array) {
        if(array==null || array.length() == 0)
            claimCountTv.setVisibility(View.INVISIBLE);
        else{
            claimCountTv.setText(String.valueOf(array.length()));
            claimCountTv.setVisibility(View.VISIBLE);
        }
    }



    class SuperMarketPagerAdapter extends FragmentPagerAdapter {

        List<Fragment> S_frags = new ArrayList<>();
        List<String> S_frags_Title = new ArrayList<>();
        Bundle bundle;
        int CAT_ID;


        public SuperMarketPagerAdapter(FragmentManager S_fm) {
            super(S_fm);


            ItemsFragment homeFragment = new ItemsFragment();
            bundle = new Bundle();
            String home_tab_url = getIntent().getExtras().getString("HOME_URL");
            bundle.putString("URL", home_tab_url);

            if(layoutType!=null && layoutType.equals("wide")) {
                bundle.putString("LayoutType", "wide");
            }
            homeFragment.setArguments(bundle);


            S_frags.add(homeFragment);
            S_frags_Title.add(null);

            String tabs_url = getIntent().getExtras().getString("TAB_URL");
            CAT_ID =  getIntent().getExtras().getInt("CAT_ID");

            MakeMeTab(tabs_url);


        }


        @Override
        public Fragment getItem(int position) {
            return S_frags.get(position);
        }

        @Override
        public int getCount() {
            return S_frags.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0)
                return "";
            else
                return S_frags_Title.get(position);
        }


        public void MakeMeTab(final String tabs_url) {

            if(prefs.getLanguage().equals("am")){
                itemTypeName = "itemTypeNameAmharic";
            }else{
                itemTypeName = "itemTypeName";
            }

            JsonArrayRequest request = new JsonArrayRequest(tabs_url, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    JSONObject currentObject;
                    try {
                        for(int i =0; i < response.length(); i++){
                            currentObject = response.getJSONObject(i);
                            String json_tabId = currentObject.getString("itemTypeId");
                            String json_tabTitle = currentObject.getString(itemTypeName);

                            String category = getIntent().getExtras().getString("category","0");
                            String tabs_url_data;

                            if(category.equals("1")){
                                 tabs_url_data = Constants.ITEM_BY_CATEGORY_BUSINESS + + CAT_ID +"/"+ json_tabId+"/";
                            }else{
                                tabs_url_data = Constants.GET_ITEM_BY_BUSINESS_AND_ITEM_CAT + CAT_ID +"/"+ json_tabId+"/";
                            }



                            ItemsFragment tab = new ItemsFragment();
                            Bundle bundle = new Bundle();
                            if(layoutType!=null && layoutType.equals("wide")) {
                                bundle.putString("LayoutType", "wide");
                            }
                            bundle.putString("URL", tabs_url_data);
                            tab.setArguments(bundle);

                            S_frags.add(tab);
                            S_frags_Title.add(json_tabTitle);


                            notifyDataSetChanged();
                            tabLayout.getTabAt(0).setIcon(R.drawable.ic_home_black_24dp);

                        }
//                        progressDialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(Home.this,"Couldn't load data",Toast.LENGTH_SHORT).show();
//                    progressDialog.dismiss();
                }
            });

            AppController.getInstance().addToRequestQueue(request, "Tag");


        }


    }
}