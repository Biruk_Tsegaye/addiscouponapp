package com.securi.addiscpn;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.securi.helpers.Constants;
import com.securi.helpers.RetailerModel;
import com.securi.helpers.SharedPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class BusinessDetails extends AppCompatActivity {
    private RetailerModel business;
    FloatingActionButton mapButton;
    TextView phonenumber,locationDetails,name,city;
    ImageView businessPhoto;
    JSONObject retailerJsonObject;
    String retailerName;
    SharedPrefs prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_details);

        Toolbar  toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        prefs = new SharedPrefs(getApplicationContext());


        if(prefs.getLanguage().equals("am")){
            retailerName = "retailerNameAmharic";
        }else{
            retailerName = "retailerName";
        }


        Intent callingIntent = getIntent();
        businessPhoto = (ImageView) findViewById(R.id.app_bar_image);
        mapButton = (FloatingActionButton) findViewById(R.id.show_map);
        phonenumber = (TextView) findViewById(R.id.phonenumber);
        city = (TextView) findViewById(R.id.city);
        locationDetails = (TextView) findViewById(R.id.location_details);
        name = (TextView) findViewById(R.id.name);

        if(callingIntent.getSerializableExtra("retailerModel")!= null){
            this.business = (RetailerModel) callingIntent.getSerializableExtra("retailerModel");
        }else if(callingIntent.getStringExtra("retailerJson") !=null){
            try {
                retailerJsonObject = new JSONObject(callingIntent.getStringExtra("retailerJson"));
                this.business = new RetailerModel();

                if(retailerJsonObject.getString("retailerNameAmharic") == null)
                    retailerName = "retailerName";

                business.setName(retailerJsonObject.getString(retailerName));
                business.setLocationDetails(retailerJsonObject.getString("locationDetails"));
                business.setPhoneNumber(retailerJsonObject.getString("phone"));
                business.setLatitude(retailerJsonObject.getDouble("latitude"));
                business.setLongitude(retailerJsonObject.getDouble("longtude"));
                business.setId(retailerJsonObject.getLong("$id"));
                business.setCity(retailerJsonObject.getString("city"));
                business.setCategoryId(retailerJsonObject.getLong("categoryId"));

                long category = retailerJsonObject.getJSONObject("retailerCategory").getLong("categoryId");

                if(category == 1){
                    business.setRetailerCategory("Supermarket");
                }else if(category == 3){
                    business.setRetailerCategory("Restaurants");

                }else if(category == 5){
                    business.setRetailerCategory("Spas");

                }else if(category == 7){
                    business.setRetailerCategory("Travel/Hotel");

                }else if(category == 9){
                    business.setRetailerCategory("Event");

                }else if(category == 11){
                    business.setRetailerCategory("Getaway");

                }else if(category == 13){
                    business.setRetailerCategory("Fitness");

                }else if(category == 15){
                    business.setRetailerCategory("Other");

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent googleMapsIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:" + business.getLatitude() + "," + business.getLongitude() ));
//                googleMapsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                if(googleMapsIntent.resolveActivity(getApplicationContext().getPackageManager())!= null){
//                    getApplicationContext().startActivity(googleMapsIntent);
//                }else{
//                    CpnUtils.ShowErrorDialog(getApplication(),"Error","You don't have google maps installed on your device");
//                }
                Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                intent.putExtra("latitude",business.getLatitude());
                intent.putExtra("longitude",business.getLongitude());
                intent.putExtra("name",business.getName());
                startActivity(intent);
            }
        });
//        String phn =  business.getPhoneNumber();

        if(business.getPhoneNumber() == null || business.getPhoneNumber().equals("null"))
            phonenumber.setText(getResources().getString(R.string.phone_not_found));
        else
            phonenumber.setText(business.getPhoneNumber());

        if(business.getCity() == null || business.getCity().equals("null"))
            city.setText(getResources().getString(R.string.city_not_found));

        else
            city.setText(business.getCity());

        if(business.getLocationDetails() == null || business.getLocationDetails().equals("null"))
            locationDetails.setText(getResources().getString(R.string.location_not_found));
        else
            locationDetails.setText(business.getLocationDetails());

        AssetManager assetManager = getApplicationContext().getAssets();

        Typeface cursiveFont = Typeface.createFromAsset(assetManager,String.format(Locale.US, "fonts/%s", "dancing_script.ttf"));
        name.setTypeface(cursiveFont);
        getSupportActionBar().setTitle(business.getName() != "null"? business.getName().replace('_',' '):getResources().getString(R.string.app_name2));
        name.setText(business.getName() != "null"? business.getName().replace('_',' '):"");

        String imageUrl = Constants.RETAILER_IMAGE + business.getPicName();
        Glide.with(getApplicationContext()).load(imageUrl).into(businessPhoto);

//        ImageLoader imageLoader = AppController.getInstance().getImageLoader();

//        businessPhoto.setImageUrl(imageUrl,imageLoader);

    }
}
