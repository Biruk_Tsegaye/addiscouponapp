package com.securi.addiscpn;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.securi.helpers.Constants;
import com.securi.helpers.SharedPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bk on 9/4/2017.
 */

public class SmsReceiver extends BroadcastReceiver {

    SharedPrefs prefs;
    String messageBody = "", sendersNo;
    SmsMessage[] messageList;
    public static String messageMode;
    Button Confirm;
    EditText edtCode;
    String VerificationCode;
    boolean IsActive;
    String ServerCode;
    @Override
    public void onReceive(final Context context, Intent intent) {

        Bundle bundle = intent.getExtras();
        Object [] objects = (Object[]) bundle.get("pdus");
        messageList = new SmsMessage[objects.length];
        prefs = new SharedPrefs(context);

        for (int i = 0; i < objects.length; i++){
            messageList[i] = SmsMessage.createFromPdu((byte[])objects[i]);
            messageBody +=  messageList[i].getMessageBody().trim();
            sendersNo = messageList[0].getOriginatingAddress();
        }

        try {
            if (messageBody.startsWith("Addis Coupon")) {

                messageBody = messageBody.substring(13);
                VerificationCode = messageBody;

                Intent i = new Intent();
                i.setClassName("com.securi.addiscpn", "com.securi.addiscpn.Verification");
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);

                JSONObject obj = new JSONObject();

                try {
                    obj.put("userName", prefs.getUserName());
                    obj.put("customerId", prefs.getUserId());
                    obj.put("userKey", "P@ssC0de123");
                    obj.put("isActive", true);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String tag_json_obj = "json_obj_req";
                String url = Constants.BASE_URL + "Api/customer/AddCustomer";

                JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                        url, obj,
                        new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                Log.d("WEB RESPONSE-QQQQQQ", response.toString());
                                try {
                                    prefs.setUserAsRegistered();
                                    prefs.saveUserName(response.getString("userName"));
                                    prefs.saveUserId(response.getInt("customerId"));

                                    IsActive = response.getBoolean("isActive");
                                    ServerCode = response.getString("verificationCode");


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                Boolean Active = IsActive;
                                prefs.setIsActive(Active);


                                Toast.makeText(context, R.string.registration_success , Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent();
                                    i.setClassName("com.securi.addiscpn", "com.securi.addiscpn.HomePage");
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    context.startActivity(i);

                            }

                        }, new Response.ErrorListener() {


                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("WEB RESPONSE-QQQQQQ", "Error: " + error.getMessage());
                        Toast.makeText(context, R.string.couldnt_connect_to_sever, Toast.LENGTH_SHORT).show();
                    }
                })
                {

                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("userName", prefs.getUserName());
                        params.put("phone", String.valueOf(prefs.getUserId()));
                        params.put("userKey", "P@ssC0de123");
                        params.put("VerificationCode", VerificationCode);
                        params.put("IsActive", String.valueOf(true));
                        return params;
                    }

                };

                jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(30000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

                Toast.makeText(context, messageBody, Toast.LENGTH_LONG).show();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
