package com.securi.addiscpn;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.securi.helpers.SharedPrefs;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Help extends AppCompatActivity implements View.OnClickListener{
    ViewPager pager;
    SeekBar seekBar;
    Button skip;
    FrameLayout welcomeScreen;
    SharedPrefs sharedPrefs;
    Button yes,no;
    ImageButton next,previous;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        pager = (ViewPager) findViewById(R.id.pager);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        skip = (Button) findViewById(R.id.skip);
        welcomeScreen = (FrameLayout) findViewById(R.id.welcome);

        yes = (Button) findViewById(R.id.yes_button);
        no = (Button) findViewById(R.id.no_button);
        next = (ImageButton) findViewById(R.id.next);
        previous = (ImageButton) findViewById(R.id.previous);


        no.setOnClickListener(this);
        skip.setOnClickListener(this);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                welcomeScreen.setVisibility(View.GONE);
                next.setVisibility(View.VISIBLE);
            }
        });
        sharedPrefs = new SharedPrefs(getApplicationContext());

        TextView amharic = (TextView)findViewById(R.id.lang_amharic);
        amharic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Configuration config = getBaseContext().getResources().getConfiguration();
                    Locale locale = new Locale("am");
                    Locale.setDefault(locale);
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                    sharedPrefs.setLanguage("am");
                    recreate();
            }
        });

        TextView english = (TextView)findViewById(R.id.lang_english);
        english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Configuration config = getBaseContext().getResources().getConfiguration();
                Locale locale = new Locale("en");
                Locale.setDefault(locale);
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                sharedPrefs.setLanguage("en");
                recreate();
            }
        });

        if(sharedPrefs.getRegistrationState()){
            welcomeScreen.setVisibility(View.GONE);
        }




        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                pager.setCurrentItem(progress);
                if(progress == 6){
                    skip.setText(getResources().getString(R.string.done));
                }else{
                    skip.setText(getResources().getString(R.string.skip));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });

        pager.setAdapter(new HelpPagerAdapter(getSupportFragmentManager()));
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                seekBar.setProgress(position);
                if(position == 6){
                    SharedPrefs prefs = new SharedPrefs(getApplicationContext());
                    prefs.setWalkThroughShown(true);
                    next.setVisibility(View.INVISIBLE);
                }else{
                    next.setVisibility(View.VISIBLE);
                }

                if(position == 0){
                    previous.setVisibility(View.INVISIBLE);
                }else{
                    previous.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(pager.getCurrentItem() + 1);
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(pager.getCurrentItem() - 1);
            }
        });
    }

    @Override
    public void onClick(View v) {
        sharedPrefs.setWalkThroughShown(true);
        if(sharedPrefs.getRegistrationState()){
            Intent categoriesIntent = new Intent(getBaseContext(), HomePage.class);
            categoriesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(categoriesIntent);
            finish();
        }else{
            Intent categoriesIntent = new Intent(getBaseContext(), Register.class);
            categoriesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(categoriesIntent);
            finish();
        }
    }

    class HelpPagerAdapter extends FragmentPagerAdapter{
        List<Fragment> fragmentList = new ArrayList<>();

        public HelpPagerAdapter(FragmentManager fm) {
            super(fm);
            int[]ids = {R.layout.fragment_help_step1,
                    R.layout.fragment_help_step2,
                    R.layout.fragment_help_step3,
                    R.layout.fragment_help_step4,
                    R.layout.fragment_help_step5,
                    R.layout.fragment_help_step6,
                    R.layout.fragment_help_step7};

            for(int i =0; i <7;i++){
                HelpFragment helpFragment = new HelpFragment();
                helpFragment.setPager(pager);
                Bundle bundle = new Bundle();
                bundle.putInt("layout_id",ids[i]);
                helpFragment.setArguments(bundle);
                fragmentList.add(helpFragment);
            }

        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

    }
}
