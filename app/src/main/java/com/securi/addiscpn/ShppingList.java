package com.securi.addiscpn;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.securi.helpers.ClaimsModel;
import com.securi.helpers.ListAdapter;
import com.securi.helpers.ShoppingListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShppingList extends Activity {
    ViewGroup header;
    String headers = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shopping_list);

        Button btnBack = (Button) findViewById(R.id.btn_Exit);
        Bundle bundle = getIntent().getExtras();

        ListView lstClaims = (ListView) findViewById(R.id.lst_shoppingList);

        if(bundle != null){
            String barcode = bundle.getString("BC");
            List<ClaimsModel> claimsList = ClaimsModel.findWithQuery(ClaimsModel.class,
                            "SELECT * FROM claims_model where claim_code=? order by retailor_name", barcode);

            for (int j = 0; j < claimsList.size(); j++){
                try {
                    if(!headers.equals(claimsList.get(j).GetRetailorName())){
                        LayoutInflater inflater = getLayoutInflater();
                        headers = claimsList.get(j).GetRetailorName();
                        header = (ViewGroup) inflater.inflate(R.layout.section_header, lstClaims, false);
                        TextView name = (TextView) header.findViewById(R.id.section_header);
                        name.setText(claimsList.get(j).GetRetailorName());
                        header.setClickable(false);

                        ShoppingListAdapter adapter = new ShoppingListAdapter(this, claimsList);
                        lstClaims.setAdapter(adapter);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();



            }
        });

    }

}

