package com.securi.addiscpn;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.securi.helpers.Constants;
import com.securi.helpers.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class Intro extends AppCompatActivity {
    SharedPrefs prefs;

    //SystemParams
    double minimumAppVersion, CurrentAppVersion;
    boolean IsPromoCodeActive, IsReferalProgActive;

    int UserId;
    private LinearLayout layoutToAdd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro);

        prefs = new SharedPrefs(Intro.this);


        Configuration config = getBaseContext().getResources().getConfiguration();
        if (! "".equals(prefs.getLanguage()) && ! config.locale.getLanguage().equals(prefs.getLanguage())) {
            Locale locale = new Locale(prefs.getLanguage());
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
            recreate();
        }


        //prefs.saveClaimedItems(null);
        IntroPageDelay();

    }

    void IntroPageDelay(){
        new Thread(new Runnable() {
            @Override
            public void run() {

                if(prefs == null){
                    UserId = 0;
                }else{
                    UserId = prefs.getUserId();
                }
                //checking system params
                String url = Constants.BASE_URL + "api/AppVersionsapilatest/" + UserId;

                //String url = Constants.BASE_URL_TEST  + "api/AppVersionsapi/";
                JsonArrayRequest req = new JsonArrayRequest(url,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {

                                try {

                                    for (int i = 0; i < response.length(); i++) {

                                        JSONObject obj = (JSONObject) response.get(i);

                                        minimumAppVersion = obj.getDouble("minimumAppVerssion");
                                        CurrentAppVersion = obj.getDouble("currentAppVerssion");
                                        IsPromoCodeActive = obj.getBoolean("isPromoCodeActive");
                                        IsReferalProgActive = obj.getBoolean("isReferalProgActive");

                                        prefs.setMinimumVersion(minimumAppVersion);
                                        prefs.setPromoCodeActive(IsPromoCodeActive);
                                        prefs.setCurrentAppVersion(CurrentAppVersion);
                                        prefs.setReferalProgActive(IsReferalProgActive);

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();

                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Error: " + error.getMessage());

                    }
                });

                AppController.getInstance().addToRequestQueue(req);

                try {
                    Thread.sleep(2000);


                    if (Constants.CURRENT_VERSION < prefs.getminimumVersion() || Constants.CURRENT_VERSION < prefs.getCurrentAppVersion()){

                        Intent intent = new Intent(Intro.this, Update.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();

                        Thread.currentThread().interrupt();

                        return;
                    }

                    if (prefs.IsActive()){
                        Intent goHome = new Intent(Intro.this, HomePage.class);
                        startActivity(goHome);
                    }
                    if (!prefs.IsActive()) {
                        Intent goHome = new Intent(Intro.this, Register.class);
                        startActivity(goHome);
                    }if(!prefs.isWalkThroughShown()) {
                        Intent help = new Intent(Intro.this, Help.class);
                        startActivity(help);

                    }else if (prefs.getUserName().isEmpty()) {
                        Intent registration = new Intent(Intro.this, Register.class);
                        startActivity(registration);
                    }
                    prefs.setWalkThroughShown(true);

                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        Thread.currentThread().interrupt();
    }

}
