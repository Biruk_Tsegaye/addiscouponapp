package com.securi.addiscpn;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Paint;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.securi.helpers.ClaimBuffer;
import com.securi.helpers.GridAdapter;

import org.json.JSONArray;


/**
 * Created by Level1 on 7/29/2016.
 */
public class Search extends AppCompatActivity implements ClaimBuffer.ClaimChangeListener{
    SearchView edtSearch;
    String searchName;
    ItemsFragment fragment;
    Toolbar toolbar;
    TextView noResultsFound;
    Button fab;
    EditText searchBox;
    ImageButton searchButton;
    GridAdapter gridAdapter;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);

        fragment = new ItemsFragment();
        Bundle args = new Bundle();
        args.putString("type","search");
//        args.putString("URL",.);
        fragment.setArguments(args);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.item_fragment,fragment).commit();
        searchBox = (EditText) findViewById(R.id.search_box);
        searchButton = (ImageButton)findViewById(R.id.search_button);
        fragment.setSearchFragment(true);

        noResultsFound = (TextView) findViewById(R.id.no_results_found);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.search(searchBox.getText().toString());
            }
        });

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        fab = (Button)fragment.getFab();
        GridAdapter fragmentGridAdapter = fragment.getAdapter();
    }

    public GridAdapter getGridAdapter() {
        return gridAdapter;
    }

    public void setGridAdapter(GridAdapter gridAdapter) {
        this.gridAdapter = gridAdapter;
    }

    @Override
    public void onCLaimChanged(JSONArray array) {

    }
}
