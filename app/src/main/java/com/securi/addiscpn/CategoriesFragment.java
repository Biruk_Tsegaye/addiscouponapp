package com.securi.addiscpn;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.securi.helpers.CategoriesAdapter;

public class CategoriesFragment extends Fragment {
    RecyclerView categoriesRecyclerView;
    CategoriesAdapter adapter;
    Toolbar toolbar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_categories,container,false);
        categoriesRecyclerView = (RecyclerView) view.findViewById(R.id.categories_recycler_view);

        adapter = new CategoriesAdapter(getContext());
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;

        if(dpWidth >= 680){
            categoriesRecyclerView.setLayoutManager(new GridLayoutManager(getContext(),3));
        }else{
            categoriesRecyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));

        }
        categoriesRecyclerView.setAdapter(adapter);
        return view;
    }


}
