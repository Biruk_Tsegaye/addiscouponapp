package com.securi.addiscpn;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.securi.helpers.ClaimsModel;
import com.securi.helpers.ListAdapter;
import com.securi.helpers.SharedPrefs;

/**
 * Created by Level1 on 8/1/2016.
 */
public class BarcodeFactory extends Activity {
    TextView txtClaimedDate, txtBarcode;
    SharedPrefs prefs;
    String barcode = "000000";
    String claimDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.barcode_factory);

        prefs = new SharedPrefs(BarcodeFactory.this);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            barcode = String.valueOf(bundle.getInt("TICKET"));
            claimDate = bundle.getString("DATE");
            for(int i=0; i< 10-barcode.length(); i++){
                barcode = "0"+barcode;
            }

        }

        Bitmap bitmap = null;
        ImageView iv = (ImageView) findViewById(R.id.img_barcode);

        try {

            bitmap = encodeAsBitmap(barcode, BarcodeFormat.CODE_128, 600, 300);
            iv.setImageBitmap(bitmap);

        } catch (WriterException e) {
            e.printStackTrace();
        }

        txtClaimedDate = (TextView) findViewById(R.id.txt_date);

        txtBarcode = (TextView) findViewById(R.id.txt_barcodeDigits);

        txtClaimedDate.append(claimDate);
        txtBarcode.setGravity(Gravity.CENTER_HORIZONTAL);
        txtBarcode.setText(barcode);

        Button btnExit = (Button) findViewById(R.id.btn_Exit);
        Button btnItems = (Button) findViewById(R.id.btn_shoppingList);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shopList = new Intent(BarcodeFactory.this, ShppingList.class);
                shopList.putExtra("BC", barcode);
                startActivity(shopList);
            }
        });
    }

    /**************************************************************
     * getting from com.google.zxing.client.android.encode.QRCodeEncoder
     *
     * See the sites below
     * http://code.google.com/p/zxing/
     * http://code.google.com/p/zxing/source/browse/trunk/android/src/com/google/zxing/client/android/encode/EncodeActivity.java
     * http://code.google.com/p/zxing/source/browse/trunk/android/src/com/google/zxing/client/android/encode/QRCodeEncoder.java
     */

    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;

    Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }
}
