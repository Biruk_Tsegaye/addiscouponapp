package com.securi.addiscpn;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.securi.helpers.ClaimBuffer;
import com.securi.helpers.Constants;
import com.securi.helpers.GridAdapter;
import com.securi.helpers.NotificationModel;
import com.securi.helpers.SharedPrefs;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

public class HomePage extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,GridAdapter.GridButtonClicked, ClaimBuffer.ClaimChangeListener,LocationListener {
    private static final int MY_PERMISSIONS_ACCESS_FINE_LOCATION = 1;
    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager pager;
    HomePagerAdapter pagerAdapter;
    MenuItem searchMenuItem;
    SharedPrefs prefs;
    TextView notifText, claimCountTv;
    int activeNotifs, currentPage;
    ClaimBuffer buffer;
    CartIconClick listner;
    double Longtiude;
    double Latitude;
    SharedPreferences settings;
    public static final String PREFS_NAME = "Dialog";
    LocationManager locationManager;
    public static Snackbar snackbar_gps;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);



        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                Toast.makeText(getApplicationContext(),"You need to enable location to use some features",Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_ACCESS_FINE_LOCATION);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_ACCESS_FINE_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }

        prefs = new SharedPrefs(getApplicationContext());

        Configuration config = getBaseContext().getResources().getConfiguration();
        if (! "".equals(prefs.getLanguage()) && ! config.locale.getLanguage().equals(prefs.getLanguage())) {
            Locale locale = new Locale(prefs.getLanguage());
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
            recreate();
        }

        setTitle(getResources().getString(R.string.app_name2));

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        // start Notification service from the activity
        Intent intent = new Intent(getApplicationContext(),NotificationService.class);
        getApplicationContext().startService(intent);


        //check if location is enabled

        LocationManager lm = (LocationManager)getApplication().getSystemService(Context.LOCATION_SERVICE);

        snackbar_gps = Snackbar.make(toolbar,getResources().getString(R.string.snacbar_gps) ,Snackbar.LENGTH_INDEFINITE).setAction(getResources().getString(R.string.snacbar_gps_open), new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent settingsIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                settingsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplication().startActivity(settingsIntent);
            }
        });

        View yourSnackBarView = snackbar_gps.getView(); //get your snackbar view
        TextView textView = (TextView) yourSnackBarView.findViewById(android.support.design.R.id.snackbar_text); //Get reference of snackbar textview
        textView.setMaxLines(4);



        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        pager = (ViewPager) findViewById(R.id.viewPager);
        pagerAdapter = new HomePagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);

        //stop refreshing Fragments on tab change
        pager.setOffscreenPageLimit(5);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                currentPage = position;
            }

            @Override
            public void onPageSelected(int position) {
                if(position == 1){
                    currentPage = position;
                    LocationManager lm = (LocationManager)getApplication().getSystemService(Context.LOCATION_SERVICE);
                    boolean gps_enabled = false;
                    boolean network_enabled = false;

                    try {
                        gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    } catch(Exception ex) {}

                    try {
                        network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                    } catch(Exception ex) {}

                    if(!gps_enabled && !network_enabled) {
                        // notify user
                        if(!snackbar_gps.isShown())
                            snackbar_gps.show();
                    }
                }else{
                    snackbar_gps.dismiss();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabLayout.setupWithViewPager(pager);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_home_black_24dp);
        Intent callingIntent = getIntent();
        int loadTab = callingIntent.getIntExtra("load_tab",0);

        pager.setCurrentItem(loadTab);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Menu menu = navigationView.getMenu();
        MenuItem menuItem = menu.findItem(R.id.referal_code);

        SharedPrefs prefs = new SharedPrefs(this);
        menuItem.setTitle(getResources().getString(R.string.ur_referal_code) + prefs.getUserId());

        Date date = new Date(System.currentTimeMillis());
        SharedPreferences preferences = getDefaultSharedPreferences(HomePage.this);
        // SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        preferences.edit().putLong("time", date.getTime()).apply();

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(HomePage.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(HomePage.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(HomePage.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_ACCESS_FINE_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        //checkPermission();
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }




    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                            0, 0, this);

                } else {
                    Snackbar.make(toolbar,"Some features will be disabled ",Snackbar.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

        }
    }


    private void showAlert() {
        settings = getSharedPreferences(PREFS_NAME, 0);
        boolean dialogShown = settings.getBoolean("dialogShown", false);
        if (!dialogShown) {

            final AlertDialog.Builder dialog = new AlertDialog.Builder(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
            dialog.setTitle(getResources().getString(R.string.loacation_en))
                    .setMessage(getResources().getString(R.string.location_msg))
                    .setPositiveButton(getResources().getString(R.string.location_setting),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);

                                    startActivity(myIntent);
                                }
                            })

                    .setNegativeButton(getResources().getString(R.string.location_cancle), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        }
                    });
            dialog.show();
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("dialogShown", true);
            editor.commit();
        }else  {
            return;
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_redeem) {
            Intent bc = new Intent(this, MyCoupons.class);
            startActivity(bc);
        } else if (id == R.id.nav_search) {
            Intent searchIntent = new Intent(this, Search.class);
            startActivityForResult(searchIntent, 3);
        } else if (id == R.id.nav_business) {
            Intent business = new Intent(this, Retailers.class);
            startActivity(business);
        } else if (id == R.id.nav_settings) {

            if (prefs.getLanguage().equals("am")) {
                Configuration config = getBaseContext().getResources().getConfiguration();
                Locale locale = new Locale("en");
                Locale.setDefault(locale);
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                recreate();
                prefs.setLanguage("en");
            } else {
                Configuration config = getBaseContext().getResources().getConfiguration();
                Locale locale = new Locale("am");
                Locale.setDefault(locale);
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                recreate();
                prefs.setLanguage("am");
            }

        } else if (id == R.id.nav_aboutUs) {
            startActivity(new Intent(this, About.class));
        } else if (id == R.id.nav_help) {
            startActivity(new Intent(this, Help.class));
        }else if(id == R.id.nav_facebook){
            Uri uri = Uri.parse("http://facebook.com/addiscoupon");
            Intent insta = new Intent(Intent.ACTION_VIEW, uri);
            insta.setPackage("com.facebook.android");

            if (isIntentAvailable(getApplicationContext(), insta)){
                startActivity(insta);
            } else{
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/addiscoupon")));
            }

        }else if(id == R.id.nav_instagram){
            Uri uri = Uri.parse("http://instagram.com/_u/addis_coupon");
            Intent insta = new Intent(Intent.ACTION_VIEW, uri);
            insta.setPackage("com.instagram.android");

            if (isIntentAvailable(getApplicationContext(), insta)){
                startActivity(insta);
            } else{
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/addiscoupon")));
            }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean isIntentAvailable(Context ctx, Intent intent){
        final PackageManager packageManager = ctx.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    @Override
    public int onGridDetailButtonClicked(int position) {
//        ShowItemDetailsDialog(position);
        return 0;
    }

    @Override
    public int onGridAddButtonClicked(int position, int itemId) {
        return 0;
    }


    @Override
    public void onCLaimChanged(JSONArray array) {
        if(array==null || array.length() == 0)
            claimCountTv.setVisibility(View.INVISIBLE);
        else{
            claimCountTv.setText(String.valueOf(array.length()));
            claimCountTv.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        Latitude = location.getLatitude();
        Longtiude = location.getLongitude();
        String msg = "Latitude: " + Latitude
                + "Longitude: " + Longtiude;
//        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
        SharedPrefs prefs = new SharedPrefs(getApplicationContext());
        prefs.setUserLocation((float) Latitude, (float)Longtiude);



        if(pagerAdapter != null ){
            BusinessesFragment businessesFragment = (BusinessesFragment)pagerAdapter.getItem(1);
            businessesFragment.refresh();
        }
        // /send lat and long to server
//        String tag_json_obj = "json_obj_req";
//        String  url = Constants.BASE_URL_TEST + "Api/Retailer/GetnearbylocationAll/" + Latitude + "/" + Longtiude + "/";

//        JsonArrayRequest req = new JsonArrayRequest(Request.Method.GET, url ,
//                new Response.Listener<JSONArray>() {
//                    @Override
//                    public void onResponse(JSONArray response) {
//                        Log.d("Response", response.toString());
//
//                        try {
//                            // Parsing json array response
//                            // loop through each json object
//                            for (int i = 0; i < response.length(); i++) {
//
//                                JSONObject obj = (JSONObject) response
//                                        .get(i);
//                                Toast.makeText(HomePage.this, obj.getString("retailerName"), Toast.LENGTH_SHORT).show();
//
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            Toast.makeText(getApplicationContext(),
//                                    "Error: " + e.getMessage(),
//                                    Toast.LENGTH_LONG).show();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                VolleyLog.d("Response", "Error: " + error.getMessage());
//                Toast.makeText(getApplicationContext(),
//                        error.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });


//        req.setRetryPolicy(new DefaultRetryPolicy(30000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        AppController.getInstance().addToRequestQueue(req, tag_json_obj);

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {
//        Toast.makeText(getApplicationContext(),"Provider Enabled",Toast.LENGTH_LONG).show();
//        if(pager.getCurrentItem() == 1){
//            ((BusinessesFragment)((HomePagerAdapter)pager.getAdapter()).getItem(1)).refresh();
//        }
    }

    @Override
    public void onProviderDisabled(String s) {

//        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//        startActivity(intent);
        showAlert();
//        Toast.makeText(getBaseContext(), "Gps is turned off!! ",Toast.LENGTH_SHORT).show();
    }

//    @Override
//    public boolean onNavigationItemSelected(MenuItem menuItem) {
//        Log.d("Item","Clicked");
//        return true;
//    }


    class HomePagerAdapter extends FragmentPagerAdapter{
        List<Fragment> frags = new ArrayList<>();

        public HomePagerAdapter(FragmentManager fm) {
            super(fm);
            Bundle bundle;
            CategoriesFragment  categoriesFragment = new CategoriesFragment();

            ItemsFragment trendingFragment = new ItemsFragment();
            bundle = new Bundle();
            bundle.putString("URL", Constants.TREDING_ITEMS_URL);
//            bundle.putString("LayoutType","wide");
            trendingFragment.setArguments(bundle);

            ItemsFragment specialDeals = new ItemsFragment();
            bundle = new Bundle();
            bundle.putString("URL", Constants.SPECIAL_OFFERS_URL);
            specialDeals.setArguments(bundle);

            BusinessesFragment nearby = new BusinessesFragment();
            bundle = new Bundle();
            // bundle.putString("URL", Constants.SPECIAL_OFFERS_URL);
            bundle.putString("type","nearby");
            nearby.setArguments(bundle);



            frags.add(categoriesFragment);
            frags.add(nearby);
            frags.add(trendingFragment);
            frags.add(specialDeals);

//            trendingFragment.LoadItemsFromWeb(0,false);
//            specialDeals.LoadItemsFromWeb(0,false);

        }


        @Override
        public Fragment getItem(int position) {
            return frags.get(position);
        }

        @Override
        public int getCount() {
            return frags.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if(position == 0){
                return "";
            }else if(position == 1){
                return getResources().getString(R.string.tab_nearby);
            }else if(position == 2){
                return getResources().getString(R.string.tab_trending);
            }else{
                return getResources().getString(R.string.tab_specialdeals);

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);

        MenuItem itemNotif = menu.findItem(R.id.action_notification);
        MenuItemCompat.setActionView(itemNotif, R.layout.style_actionbar_badge);
        RelativeLayout notifCount = (RelativeLayout) MenuItemCompat.getActionView(itemNotif);

        notifText = (TextView) notifCount.findViewById(R.id.actionbar_notifcation_textview);

        ImageView notif_Image = (ImageView)notifCount.findViewById(R.id.notificationImage);
        notif_Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomePage.this, NotificationActivity.class));
            }
        });

        List<NotificationModel> notifList = NotificationModel.find(NotificationModel.class, "is_seen=?", "0");

        if (!notifList.isEmpty()) {
            for (int i = 0; i < notifList.size(); i++) {

                activeNotifs = notifList.size();
            }
        }

        if(activeNotifs == 0 || notifList.isEmpty())
            notifText.setVisibility(View.INVISIBLE);
        else if(activeNotifs > 99){
            notifText.setText(String.valueOf(99));
        }
        else
            notifText.setVisibility(View.VISIBLE);


        notifText.setText(String.valueOf(activeNotifs));
        notifText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(HomePage.this, NotificationActivity.class));

            }
        });

        MenuItem itemCount = menu.findItem(R.id.action_redeem);
        MenuItemCompat.setActionView(itemCount, R.layout.style_actionbar_cart_count);
        FrameLayout claimCount = (FrameLayout) MenuItemCompat.getActionView(itemCount);

        ImageView imgActionCart = (ImageView) claimCount.findViewById(R.id.imagebuttonid);
        claimCountTv = (TextView) claimCount.findViewById(R.id.actionbar_notifcation_textview);
        final int prevClaims = ClaimBuffer.getInstance().GetClaims(HomePage.this).length();
        if (prevClaims <= 0)
            claimCountTv.setVisibility(View.INVISIBLE);
        else{
            claimCountTv.setText(String.valueOf(prevClaims));
            claimCountTv.setVisibility(View.VISIBLE);
        }

        imgActionCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                JSONArray claims = ClaimBuffer.getInstance().GetClaims(HomePage.this);
//                if(claims == null || claims.length() <= 0){
//                    CpnUtils.ShowErrorDialog(HomePage.this, "Warning!", " No item selected. " +
//                            "You need to select at least one item to get your coupon.");
//                }else{
//                    Intent summery = new Intent(HomePage.this, ClaimSummery.class);
//                    summery.putExtra("CLAIMS", claims.toString());
//                    startActivityForResult(summery, 2);
//                }
                try {
                    if (currentPage > 1){
                        ItemsFragment currentFrag;
                        if(pager.getCurrentItem() > 0){
                            currentFrag = (ItemsFragment) pagerAdapter.getItem(pager.getCurrentItem());
                            currentFrag.ProcessClaims();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_redeem) {
            if(pager.getCurrentItem()>0){
                ItemsFragment frag = (ItemsFragment) pagerAdapter.getItem(pager.getCurrentItem());
                frag.ProcessClaims();
            }
            return true;
        } else if (id == R.id.action_search) {
            Intent searchIntent = new Intent(HomePage.this, Search.class);
            startActivity(searchIntent);
            searchMenuItem = item;
            return true;
        } else if(id== R.id.action_notification){
            Intent notificationIntent = new Intent(HomePage.this, NotificationActivity.class);
            startActivity(notificationIntent);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
                ClaimBuffer.getInstance().ResetClaim(HomePage.this);
                ClaimBuffer.getInstance().BroadcastDataChange(HomePage.this);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(ClaimBuffer.getInstance().GetClaims(HomePage.this).length() > 0 ){
            AlertDialog.Builder builder;
            builder = new AlertDialog.Builder(new ContextThemeWrapper(HomePage.this, R.style.AlertDialogCustom));
            builder.setMessage(R.string.ur_going_to_lose_items)
                    .setIcon(getApplicationContext().getResources().getDrawable(R.drawable.dialog_alert))
                    .setTitle(R.string.ur_warning)
                    .setPositiveButton(R.string.ur_warning_yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            ClaimBuffer.getInstance().ResetClaim(HomePage.this);
                            finish();
                            System.exit(0);
                        }
                    })

                    .setNegativeButton(R.string.ur_warning_no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    }).create();
            builder.show();


        }else


            finish();
        System.exit(0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("dialogShown", false);
        editor.commit();

    }



    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            ClaimBuffer.getInstance().BroadcastDataChange(HomePage.this);
            onCLaimChanged(ClaimBuffer.getInstance().GetClaims(HomePage.this));
        }
    }

    public interface CartIconClick {
        public void onCartIconClicked();
    }


}
