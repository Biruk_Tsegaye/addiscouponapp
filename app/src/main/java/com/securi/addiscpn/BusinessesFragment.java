package com.securi.addiscpn;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.securi.helpers.RetailersAdapter;

public class BusinessesFragment extends Fragment{
    RecyclerView retailersRecyclerView;
    RetailersAdapter adapter;
    SwipeRefreshLayout refresh;
    Button refreshButton;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_businesses,container,false);
        String type = getArguments().getString("type");
        refreshButton = (Button) view.findViewById(R.id.refresh_button);

        refresh = (SwipeRefreshLayout)view.findViewById(R.id.swipeRefresh);
        refresh.setColorSchemeResources(R.color.colorAccent);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
                refresh.setRefreshing(false);
            }
        });

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh();
            }
        });

        if(type.equalsIgnoreCase("nearby")){
            adapter = new RetailersAdapter(getContext(),"nearby");
        }else{
            adapter = new RetailersAdapter(getContext(),"all");
        }

        if(adapter.getItemCount()==0){
            refreshButton.setVisibility(View.VISIBLE);
        }



        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                if(adapter.getItemCount()==0){
                    refreshButton.setVisibility(View.VISIBLE);
                }else{
                    refreshButton.setVisibility(View.GONE);
                    HomePage.snackbar_gps.dismiss();
                }
            }
        });

        retailersRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        retailersRecyclerView.setAdapter(adapter);

//        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
//        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
//        int noOfColumns = (int) (dpWidth / 180);

        retailersRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }

    public void refresh(){
        adapter.loadItemsFromWeb();
        adapter.notifyDataSetChanged();

    }

}
