package com.securi.addiscpn;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.securi.helpers.ClaimsModel;
import com.securi.helpers.CpnUtils;
import com.securi.helpers.TicketListAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MyCoupons extends AppCompatActivity implements TicketListAdapter.CouponClicked {
    List<ClaimsModel> stagingList;
    TicketListAdapter adapter;
    ViewGroup header;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ticket_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ClaimsModel tempModel = new ClaimsModel();

        stagingList = new ArrayList<ClaimsModel>();
        //List<ClaimsModel> claimsList = ClaimsModel.listAll(ClaimsModel.class);
        List<ClaimsModel> claimsList = ClaimsModel.findWithQuery(ClaimsModel.class, "SELECT * FROM claims_model ORDER BY id DESC", null);

        // - - - - GROUP BY TICKET NO. - - - \\
        if(claimsList.size() > 0){
            int claimCodeTemp = -1;
            for(int i=0; i<claimsList.size(); i++){

                if(claimsList.get(i).getClaimCode() == claimCodeTemp){
                    tempModel.setItemName(tempModel.getItemName() + ", " + claimsList.get(i).getItemName());
                }else{
                    if(i != 0)
                        stagingList.add(tempModel);
                    //---Load New value---\\
                    tempModel = claimsList.get(i);
                    claimCodeTemp = tempModel.getClaimCode();
                }
            }
            stagingList.add(tempModel);

        }else{
            TextView label = (TextView) findViewById(R.id.top_label);
            label.setText("You haven't taken any coupon yet.");
        }

        TicketListAdapter adapter = new TicketListAdapter(MyCoupons.this, stagingList);
        ListView lstTickets = (ListView) findViewById(R.id.lst_ticketList);
        lstTickets.setAdapter(adapter);


        lstTickets.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /*Toast.makeText(getApplicationContext(),"Item clicked!",Toast.LENGTH_SHORT).show();
                Intent barcode = new Intent(MyCoupons.this, BarcodeFactory.class);
                barcode.putExtra("TICKET", stagingList.get(position).getClaimCode());
                barcode.putExtra("DATE", stagingList.get(position).getClaimDate().toString());

                startActivity(barcode);*/
            }
        });
    }

//    public void onBackPressed() {
//        Intent goHome = new Intent(this, HomePage.class);
//        finish();
//        startActivityForResult(goHome, 1);
//
//    }


    @Override
    public void couponClicked(int position) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");

        Intent barcode = new Intent(MyCoupons.this, BarcodeFactory.class);
        barcode.putExtra("TICKET", stagingList.get(position).getClaimCode());
        barcode.putExtra("DATE", simpleDateFormat.format(stagingList.get(position).getClaimDate().getTime()) + "  " + CpnUtils.ReformateLocalDate(stagingList.get(position).getClaimDate()));

        startActivity(barcode);
    }


}