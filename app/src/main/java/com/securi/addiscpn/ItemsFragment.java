package com.securi.addiscpn;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.securi.helpers.ClaimBuffer;
import com.securi.helpers.Constants;
import com.securi.helpers.CpnUtils;
import com.securi.helpers.GridAdapter;
import com.securi.helpers.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Michael on 7/29/2017.
 */

public class ItemsFragment extends Fragment implements NavigationView.OnNavigationItemSelectedListener, GridAdapter.GridButtonClicked, ClaimBuffer.ClaimChangeListener {
    View view;
    Button btnReload;
    GridView gridItems;
    GridAdapter adapter;
    public JSONArray data = new JSONArray();
    JSONArray claims;
    JSONArray storedClaims;
    View selectedView = null;
    RelativeLayout RLY_Error;
    int position, page = 0, scrollStopped;
    SharedPrefs prefs;
    String searchValue, url, urlParam;
    boolean oldDataIsSearch;
    SwipeRefreshLayout refresh;
    TextView noResultsFound;
    //    ImageView sadFace;
    private int visibleThreshold = 1;
    private int currentPage = 0;
    private int previousTotalItemCount = 0;
    private boolean loading = true, animationStarted = false;
    private int startingPageIndex = 0;

    private String layoutType;
    private Button fab;
    private boolean searchFragment = false, refreshing = false, actionAdd = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_items,container,false);
        //Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        Bundle bundle = getArguments();
        if(bundle != null){
            urlParam = bundle.getString("URL");
            layoutType = bundle.getString("LayoutType");

            if(bundle.getString("type")!= null && bundle.getString("type").equals("search")){
                setSearchFragment(true);
            }
        }
        noResultsFound = (TextView) view.findViewById(R.id.no_results_found);
//        sadFace = (ImageView) view.findViewById(R.id.sad_face);

        refresh = (SwipeRefreshLayout)view.findViewById(R.id.swipeRefresh);
        refresh.setColorSchemeResources(R.color.colorAccent);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshing = true;
                LoadItemsFromWeb(page, false);
                refresh.setRefreshing(false);
            }
        });

        searchValue = "";
        claims = ClaimBuffer.getInstance().GetClaims(getActivity());
        prefs = new SharedPrefs(getContext());
        gridItems = (GridView)view.findViewById(R.id.grid_Items);


        if(layoutType!=null && layoutType.equals("wide")){
            gridItems.setNumColumns(1);
            actionAdd = false;
        }

        btnReload = (Button) view.findViewById(R.id.btn_reload);
        btnReload.setText("Reload");

        RLY_Error = (RelativeLayout) view.findViewById(R.id.RLY_connectError);
        fab = (Button) view.findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProcessClaims();
            }
        });

        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoadItemsFromWeb(0, false);
            }
        });

        gridItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int _position, long id) {

                try {
                    int itemId = data.getJSONObject(_position).getInt("itemId");
                    itemClicked(_position, itemId);
                    //AnimateAddingEffect(view);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        gridItems.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                //scrollStopped = scrollState;
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                int topRowVerticalPosition = (gridItems == null || gridItems.getChildCount() == 0) ? 0 : gridItems.getChildAt(0).getTop();
                refresh.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);

                if (totalItemCount < previousTotalItemCount) {
                    currentPage = startingPageIndex;
                    previousTotalItemCount = totalItemCount;
                    if (totalItemCount == 0) { loading = true; }
                }
                // If it's still loading, we check to see if the dataset count has
                // changed, if so we conclude it has finished loading and update the current page
                // number and total item count.
                if (loading && (totalItemCount > previousTotalItemCount)) {
                    loading = false;
                    previousTotalItemCount = totalItemCount;
                    currentPage++;
                }

                // If it isn't currently loading, we check to see if we have breached
                // the visibleThreshold and need to reload more data.
                // If we do need to reload some more data, we execute onLoadMore to fetch the data.
                if (!loading && (firstVisibleItem + visibleItemCount + visibleThreshold) >= totalItemCount ) {
                    LoadItemsFromWeb(currentPage, true);
                    loading = true;
                }
            }
        });
        LoadItemsFromWeb(0,false);
        return view;
    }

    public void ProcessClaims() {
        claims = ClaimBuffer.getInstance().GetClaims(getActivity());
        if(claims == null || claims.length() <= 0){
            View dialogView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_error,null);
            TextView dialogMessage = (TextView) dialogView.findViewById(R.id.message);
            TextView dialogTitle = (TextView) dialogView.findViewById(R.id.title);

            dialogMessage.setText(R.string.no_items_selected_message);
            dialogTitle.setText(R.string.no_items_selected_title);

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            AlertDialog dialog = builder
                    .setView(dialogView)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create();
            dialog.show();

        }else{
            Intent summery = new Intent(getContext(), ClaimSummery.class);
            summery.putExtra("CLAIMS", claims.toString());
            startActivityForResult(summery, 2);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                int itemId = Integer.valueOf(data.getStringExtra("ID"));
                int _position = Integer.valueOf(data.getStringExtra("POS"));
                actionAdd = true;
                itemClicked(_position, itemId);

                /*try {
                    this.data.getJSONObject(position).put("picked", true);
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                AddToClaimsList(itemId);*/

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //ohh...! nothing is coming :(
            }
        }else if(requestCode == 2){
            if(resultCode == Activity.RESULT_OK){
                oldDataIsSearch = false;
                claims = ClaimBuffer.getInstance().ResetClaim(getActivity());
                for(int i=0; i<this.data.length(); i++) {
                    try {
                        this.data.getJSONObject(i).put("picked", false);

                        adapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else{
                ShowPickedUnpickedStatus();
            }

        } else if(requestCode == 3){
            if(resultCode == Activity.RESULT_OK){
                //claims = null;
                searchValue = data.getStringExtra("SEARCH");
                page = 0;
                LoadItemsFromWeb(page, false);
            }
        }
    }

    private void AddToClaimsList(int itemId) {
        try {
            for (int i = 0; i < data.length(); i++){
                JSONObject obj = data.getJSONObject(i);
                if(itemId == obj.getInt("itemId")){
                    claims.put(obj);
                    //TODO: **replace with claims buffer
                    ClaimBuffer.getInstance().AddNewClaim(getActivity(), obj);
                    //prefs.saveClaimedItems(claims);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        return false;
    }

    public void LoadItemsFromWeb(final int page, final boolean loadingMore) {
        RLY_Error.setVisibility(View.INVISIBLE);
        String tag_json_arry = "json_array_req";
        url = Constants.BASE_URL+ urlParam + prefs.getUserName() + "/" + page;

        Log.d("URL", url);
        if(!searchValue.isEmpty()){
            url = Constants.BASE_URL + Constants.SEARCH + prefs.getUserName() + "/"+ searchValue + "/" + page;
        }
        if(searchValue.isEmpty() && searchFragment){
            RLY_Error.setVisibility(View.INVISIBLE);
            gridItems.setVisibility(View.INVISIBLE);
            return;
        }
        final ProgressDialog pDialog = new ProgressDialog(getContext(), R.style.MyTheme);
        pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        if(!refreshing && !searchFragment) {

            pDialog.show();
        }else if(searchFragment){

            pDialog.show();
        }

        JsonArrayRequest req = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("WEB RESPONSE-QQQQQQ", response.toString());
                if(!refreshing){
                    pDialog.dismiss();
                }
                else if(searchFragment){
                    RLY_Error.setVisibility(View.INVISIBLE);
                }

                gridItems.setVisibility(View.VISIBLE);

                try {
                    for (int i = 0; i < response.length(); i++) {
                        response.getJSONObject(i).remove("$id");
                        response.getJSONObject(i).put("picked", false);
                    }

                    if (data.length() <= 0 || !loadingMore) {
                        data = response;

                        adapter = new GridAdapter(layoutType,getActivity(), data,ItemsFragment.this,GridAdapter.DEFAULT_LAYOUT);

                        //adapter.hasStableIds();
                        gridItems.setAdapter(adapter);

                    }else{
                        for (int i = 0; i < response.length(); i++) {
                            data.put(response.get(i));
                        }

                        //adapter.hasStableIds();
                        adapter.notifyDataSetChanged();
                    }
                    ShowPickedUnpickedStatus();

                    if(data.length() <= 0){
                        if(getActivity() instanceof Search ){
                            noResultsFound.setText("No results found");
//                            sadFace.setVisibility(View.VISIBLE);
                            noResultsFound.setVisibility(View.VISIBLE);
                        }else{
                            noResultsFound.setText(getResources().getString(R.string.all_sold));
                            noResultsFound.setVisibility(View.VISIBLE);
//                            sadFace.setVisibility(View.VISIBLE);
                        }
                    }else{
                        if(getActivity() instanceof Search ){
                            noResultsFound.setVisibility(View.GONE);
//                            sadFace.setVisibility(View.GONE);
                        }
                    }

                } catch (JSONException e) {
                    if(!searchFragment && !refreshing)
                        pDialog.dismiss();
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (data.length() > 0 || loadingMore) {
                    return;
                }
                gridItems.setVisibility(View.GONE);
                RLY_Error.setVisibility(View.VISIBLE);
                VolleyLog.d("ERROR RESPONSE-XXXXXX", "Error: " + error.getMessage());
                if(!searchFragment && !refreshing)
                    pDialog.hide();
            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(req, tag_json_arry);
//        pDialog.hide();
    }

    public void ShowPickedUnpickedStatus() {
        storedClaims = ClaimBuffer.getInstance().GetClaims(getActivity());
        if (storedClaims.length() > 0) {
            try {
                for (int i = 0; i < data.length(); i++) {
                    for (int j = 0; j < storedClaims.length(); j++){
                        data.getJSONObject(i).put("picked", false);
                        JSONObject storedObj = storedClaims.getJSONObject(j);
                        if(storedObj.getInt("itemId") == data.getJSONObject(i).getInt("itemId")){
                            data.getJSONObject(i).put("picked", true);
                            break;
                        }
                    }
                }
                if (adapter != null)
                    adapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else {
            for (int i = 0; i < data.length(); i++) {
                try {
                    data.getJSONObject(i).put("picked", false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            //adapter.hasStableIds();
            if (adapter != null)
                adapter.notifyDataSetChanged();
        }
    }

     /*   public void removeItem(int position){
      try {
            data.getJSONObject(position).put("picked","false");
            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }*/

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Intent callingIntent = getActivity().getIntent();
        String type = callingIntent.getStringExtra("from");
    }

    public void itemClicked(int position, int itemId ){

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AlertDialogCustom));
        final View dialogContent = LayoutInflater.from(getContext()).inflate(R.layout.dialog_success,null);
        dialogBuilder.setView(dialogContent);
        final SharedPrefs prefs = new SharedPrefs(getContext());
        AlertDialog dialog = dialogBuilder
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(((CheckBox)dialogContent.findViewById(R.id.dont_show_again)).isChecked()){
                            prefs.setShowInfoDialog(false);
                        }
                        dialog.dismiss();
                    }
                })
                .create();


        if(actionAdd || layoutType == null) {
            // --GET POSITION OF CLICKED ITEM --\\
            //-- ON OTHER INSTANCE OF THE FRAGMENT --\\
            for (int i = 0; i < data.length(); i++) {
                try {
                    if (itemId == data.getJSONObject(i).getInt("itemId")) {
                        position = i;
                        break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            //-- CHECK IF ITEM IS PICKED --//
            //-- IF IT WAS, THEN UNPICK IT--//
            try {
                if (data.getJSONObject(position).getBoolean("picked")) {
                    data.getJSONObject(position).put("picked", false);

                    for (int i = 0; i < claims.length(); i++) {
                        if (claims.getJSONObject(i).getInt("itemId") == data.getJSONObject(position).getInt("itemId")) {
                            ClaimBuffer.getInstance().RemoveClaim(getActivity(), claims.getJSONObject(i).getInt("itemId"));
                            claims.remove(i);
                        }
                    }
                } else {
                    //-- CHECK IF ITEM IS SOLDOUT--//
                    if (data.getJSONObject(position).getInt("itemQuantity") <= 0) {
                        CpnUtils utils = new CpnUtils();
                        utils.ShowErrorDialog(getContext(), "SOLDOUT!", "WThis item is already soldout. " +
                                "Please take other items till we get you more offers of this item.\nThank you!");
                        return;
                    }

                    if(new SharedPrefs(getContext()).isInfoDialogEnabled() && claims!=null && claims.length()<1){
                        dialog.show();
                    }

                    data.getJSONObject(position).put("picked", true);
                    int id = data.getJSONObject(position).getInt("itemId");
                    AddToClaimsList(id);

                }

                //adapter.hasStableIds();
                adapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            actionAdd = false;
        } else
            onGridDetailButtonClicked(position);



    }

    private void AnimateAddingEffect(final View view){
        try {
           /* if (view != null)
                ViewCompat.setTranslationZ(view, 9);*/

            LayoutInflater inflater = getActivity().getLayoutInflater();
            View myView = inflater.inflate(R.layout.style_grid_items, null);
            myView.setX(view.getX());
            myView.setY(view.getY());
            ViewCompat.setTranslationZ(myView, 999);

            int end[] = new int[2];
            getActivity().findViewById(R.id.action_redeem).getLocationOnScreen(end);
            myView.animate().x(end[0]+200).y(end[1]+350).rotationYBy(90f);

            //YoYo.with(Techniques.Shake).duration(700).playOn(getActivity().findViewById(R.id.action_redeem));
            YoYo.with(Techniques.Shake).duration(3000).playOn(myView);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                        //adapter.hasStableIds();
                        adapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
            Thread.currentThread().interrupt();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int onGridDetailButtonClicked(int position) {
        try {
            Intent details = new Intent(getContext(),Details.class);
            details.putExtra("data",data.getJSONObject(position).toString());
            details.putExtra("pos", position);
            startActivityForResult(details, 1);
        } catch (JSONException e) {
            e.printStackTrace();
            return 1;
        }
        return 0;
    }

    @Override
    public int onGridAddButtonClicked(int position, int itemId) {
        this.itemClicked(position, itemId);
        // AnimateAddingEffect(view);
        return 0;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void search(String query){
        searchValue = query;
        page = 0;
        LoadItemsFromWeb(page, false);
    }

    public Button getFab() {
        return fab;
    }
    public void setSearchFragment(boolean bool){
        this.searchFragment = bool;
    }

    public GridAdapter getAdapter(){
        return adapter;
    }

    @Override
    public void onCLaimChanged(JSONArray array) {
        ShowPickedUnpickedStatus();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_redeem : {
                return true;
            }

            case R.id.action_search : {
                return true;
            }

            default:return false;
        }
    }

/*

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        //inflater.inflate(R.menu.home, menu);

        MenuItem itemNotif = menu.findItem(R.id.action_notification);
        MenuItemCompat.setActionView(itemNotif, R.layout.style_actionbar_badge);
        RelativeLayout notifCount = (RelativeLayout) MenuItemCompat.getActionView(itemNotif);

        notifText = (TextView) notifCount.findViewById(R.id.actionbar_notifcation_textview);

        ImageView notif_Image = (ImageView)notifCount.findViewById(R.id.notificationImage);
        notif_Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), NotificationActivity.class));
            }
        });

        List<NotificationModel> notifList = NotificationModel.find(NotificationModel.class, "is_seen=?", "0");

        if (notifList != null) {
            for (int i = 0; i < notifList.size(); i++) {
                activeNotifs = +i;
            }
        }

        if(activeNotifs == 0 || notifList == null)
            notifText.setVisibility(View.INVISIBLE);
        else if(activeNotifs > 99){
            notifText.setText(String.valueOf(99));
        }
        else
            notifText.setVisibility(View.VISIBLE);


        notifText.setText(String.valueOf(activeNotifs));
        notifText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getActivity(), NotificationActivity.class));

            }
        });

        MenuItem itemCount = menu.findItem(R.id.action_redeem);
        MenuItemCompat.setActionView(itemCount, R.layout.style_actionbar_cart_count);
        FrameLayout claimCount = (FrameLayout) MenuItemCompat.getActionView(itemCount);

        ImageView imgActionCart = (ImageView) claimCount.findViewById(R.id.imagebuttonid);
        claimCountTv = (TextView) claimCount.findViewById(R.id.actionbar_notifcation_textview);
        int prevClaims = ClaimBuffer.getInstance().GetClaims(getActivity()).length();
        if (prevClaims <= 0)
            claimCountTv.setVisibility(View.INVISIBLE);
        else{
            claimCountTv.setText(String.valueOf(prevClaims));
            claimCountTv.setVisibility(View.VISIBLE);
        }

        imgActionCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: Implement click event of action bar cart
                Toast.makeText(getActivity(), "Olla", Toast.LENGTH_SHORT).show();
            }
        });


    }*/


    public void ShowMessage(String title, String message){
        AlertDialog.Builder al = new AlertDialog.Builder(getActivity());
        al.setTitle(title);
        al.setCancelable(true);
        al.setMessage(message);
        al.show();
    }
}

