package com.securi.addiscpn;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.securi.helpers.Constants;
import com.securi.helpers.SharedPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Verification extends AppCompatActivity {

    Button Confirm;
    EditText edtCode;
    String VerificationCode;
    boolean IsActive;
    String ServerCode;
    Context context;
    SharedPrefs prefs = new SharedPrefs(Verification.this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        this.setFinishOnTouchOutside(false);

        Confirm  = (Button)findViewById(R.id.btn_confirm);
        edtCode = (EditText)findViewById(R.id.edt_Vcode);

        Confirm.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               JSONObject obj = new JSONObject();

               try {
                   obj.put("userName", prefs.getUserName());
                   obj.put("customerId", prefs.getUserId());
                   obj.put("userKey", "P@ssC0de123");
                   obj.put("isActive", true);
               } catch (JSONException e) {
                   e.printStackTrace();
               }

               String tag_json_obj = "json_obj_req";
               String url = Constants.BASE_URL + "Api/customer/AddCustomer";

               JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                       url, obj,
                       new Response.Listener<JSONObject>() {

                           @Override
                           public void onResponse(JSONObject response) {
                               Log.d("WEB RESPONSE-QQQQQQ", response.toString());
                               try {
                                   prefs.setUserAsRegistered();
                                   prefs.saveUserName(response.getString("userName"));
                                   prefs.saveUserId(response.getInt("customerId"));

                                   VerificationCode = edtCode.getText().toString();

                                    IsActive = response.getBoolean("isActive");
                                    ServerCode = response.getString("verificationCode");

                               } catch (JSONException e) {
                                   e.printStackTrace();
                               }

                              String SverificationCode = ServerCode;
                               String VCode = VerificationCode;
                               Boolean Active = IsActive;
                               if (!VCode.equalsIgnoreCase(SverificationCode) || !Active){
                                   Toast.makeText(Verification.this, R.string.couldnt_connect_to_sever, Toast.LENGTH_SHORT).show();
                               }else{

                                   prefs.setIsActive(Active);
                                   Toast.makeText(Verification.this, R.string.registration_success, Toast.LENGTH_SHORT).show();
                                   startActivity(new Intent(Verification.this, HomePage.class));
                                   finish();
                               }
                           }

                       }, new Response.ErrorListener() {
                   @Override
                   public void onErrorResponse(VolleyError error) {
                       VolleyLog.d("WEB RESPONSE-QQQQQQ", "Error: " + error.getMessage());
                       Toast.makeText(Verification.this, R.string.couldnt_connect_to_sever, Toast.LENGTH_SHORT).show();
                   }
               })
               {
                   @Override
                   protected Map<String, String> getParams() {
                       Map<String, String> params = new HashMap<String, String>();
                       params.put("userName", prefs.getUserName());
                       params.put("phone", String.valueOf(prefs.getUserId()));
                       params.put("userKey", "P@ssC0de123");
                       params.put("VerificationCode", VerificationCode);
                       params.put("IsActive", String.valueOf(true));
                       return params;

                   }
               };

               jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(30000,
                       DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                       DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
               AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
           }
       });
    }
    @Override
    public void onBackPressed() {

        finish();
        System.exit(0);
    }


}
